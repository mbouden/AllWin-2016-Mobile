/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Compte;
import Entity.Fan;
import Entity.Joueur;
import Entity.Medecin;
import Entity.Membre;
import Entity.ResponsableAntiDopage;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.RadioButton;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Bilel
 */
public class InscriptionDao {

    public static String Type = "";
    TextField nom = new TextField();
    TextField prenom = new TextField();
    TextField username = new TextField();
    TextField password = new TextField();
    Container ajout = new Container();
    TextField cin = new TextField();
    TextField confirmpassword = new TextField();
    TextField email = new TextField();
    TextField Numerotelephone = new TextField();
    TextField Sexe = new TextField();
    DateSpinner date = new DateSpinner();

    public void getAdd() {
        Image fann = null;
        try {
            fann = Image.createImage("/fan.png");
        } catch (IOException ex) {
        }
        Image adm = null;
        try {
            adm = Image.createImage("/adm.png");
        } catch (IOException ex) {
        }

        Image arb = null;
        try {
            arb = Image.createImage("/play.png");
        } catch (IOException ex) {
        }

        Image bak = null;
        try {
            bak = Image.createImage("/images.jpg");
        } catch (IOException ex) {
        }

        Image play = null;
        try {
            play = Image.createImage("/joueurs.png");
        } catch (IOException ex) {
        }
        Image doc = null;
        try {
            doc = Image.createImage("/doc.png");
        } catch (IOException ex) {
        }
        RadioButton fan = new RadioButton("Fan", fann);
        RadioButton arbitre = new RadioButton("Refree", arb);
        RadioButton joueu = new RadioButton("Player", play);
        RadioButton medecin = new RadioButton("Doctor", doc);
        RadioButton responnsable = new RadioButton("Responsable", adm);
        List<Membre> fans = new ArrayList<>();
        Form SignIn = new Form("", new BoxLayout(BoxLayout.Y_AXIS));
        joueu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ajout.removeAll();
                ajout = new Container();

                ajout.add(cin);
                SignIn.addComponent(11, ajout);
            }
        });

        Container radio = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Dialog.setDefaultBlurBackgroundRadius(8);
//        SignIn.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

        nom.setHint("First Name");
        prenom.setHint("Last Name");
        username.setHint("Login");
        password.setHint("Password");
        confirmpassword.setHint("Type your password again");
        email.setHint("E-mail");
        cin.setHint("CIN");
        Numerotelephone.setHint("Phone");
        Sexe.setHint("Sexe");
//        naissance.setHint("Date Of Birth");
        Image club = null;
        try {
            club = Image.createImage("/club.png");
        } catch (IOException ex) {
        }
        Button bt = new Button("Get Started ->", club);

        SignIn.add(bak);
        SignIn.add(nom);
        SignIn.add(prenom);
        SignIn.add(username);
        SignIn.add(password);
        SignIn.add(confirmpassword);
        SignIn.add(email);
        SignIn.add(Sexe);
        SignIn.add(Numerotelephone);
        SignIn.add(date);

        medecin.setUIID("RBJ");
        responnsable.setUIID("RBJ");
        fan.setSelected(true);
        new ButtonGroup(medecin, joueu, fan, responnsable, arbitre);
        radio.add(fan);
        radio.add(joueu);
        radio.add(medecin);
        radio.add(responnsable);
        radio.add(arbitre);

        SignIn.add(radio);
        SignIn.add(bt);
        Resources r = null;
        try {
            r = Resources.open("/theme.res");
        } catch (IOException ex) {
        }
        Command cmdBack;
        Image imBa = r.getImage("back-command.png");
        cmdBack = new Command("Back", imBa);
        SignIn.getToolbar().addCommandToLeftBar(cmdBack);
        SignIn.addCommandListener(e -> {
            if (e.getCommand() == cmdBack) {
                new Authentification().connect();
            }
        });

        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                InscriptionDao.Type = "";
                ConnectionRequest connectionRequest2;
                connectionRequest2 = new ConnectionRequest() {
                    @Override
                    protected void readResponse(InputStream in) throws IOException {
                        System.out.println(in);
                        JSONParser json = new JSONParser();
                        try {
                            Reader reader = new InputStreamReader(in, "UTF-8");

                            Map<String, Object> data = json.parseJSON(reader);

                            List<Map<String, Object>> content
                                    = (List<Map<String, Object>>) data.get("root");
                            fans.clear();

                            for (Map<String, Object> obj : content) {
                                Compte c = new Compte();
                                c.setLogin((String) obj.get("usernameMembre"));
                                c.setMail(((String) obj.get("emailMembre")));
                                fans.add(new Fan(0, (String) obj.get("nomMembre"), (String) obj.get("prenomMembre"), new Date(), "Homme", c, 21942788)
                                );
                                System.out.println(fans);
                            }

                        } catch (IOException err) {
                            Log.e(err);
                        }
                    }

                    @Override
                    protected void postResponse() {
                        int x = 0;
                        Compte c = new Compte();
                        if ("".equals(nom.getText())) {
                            Dialog.show("Error !! ", "Empty First Name", "ok", null);
                            x = 0;
                        } else if ("".equals(prenom.getText())) {
                            Dialog.show("Error !! ", "Empty Last Name", "ok", null);
                            x = 0;
                        } else if ("".equals(username.getText())) {
                            Dialog.show("Error !! ", "Empty Login", "ok", null);
                            x = 0;
                        } else if ("".equals(password.getText())) {
                            Dialog.show("Error !! ", "Empty Password", "ok", null);
                            x = 0;
                        } else if ("".equals(confirmpassword.getText())) {
                            Dialog.show("Error !! ", "Confirm your Password", "ok", null);
                            x = 0;
                        } else if ("".equals(username.getText())) {
                            Dialog.show("Error !! ", "Empty Email", "ok", null);
                            x = 0;

                        } else if (password.getText().equals(confirmpassword.getText())) {
                            //System.out.println(fans);

                            for (int i = 0; i < fans.size(); i++) {
                                if (fans.get(i).getCompte().getLogin().equals((String) username.getText())) {
                                    Dialog.show("Erroor !! ", "Username Already Exist", "ok", null);
                                    x = 1;
                                }
                                if (fans.get(i).getCompte().getMail().equals(email.getText())) {
                                    Dialog.show("Erroor !! ", "Email Already Exist", "ok", null);
                                    x = 1;
                                }
                            }
                            if (x == 0) {
                                c.setLogin(username.getText());
                                c.setPasseword(password.getText());
                                c.setMail(email.getText());
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                                String dae = date.getCurrentYear() + "-" + date.getCurrentMonth() + "-" + date.getCurrentDay();
                                if (medecin.isSelected()) {
                                    Medecin F = new Medecin();
                                    F.setNom(nom.getText());
                                    F.setPrenom(prenom.getText());
                                    F.setCompte(c);
                                    F.setCin(00);
                                    
                                    F.setSexe(Sexe.getText());
                                    F.setNumTel(Integer.parseInt(Numerotelephone.getText()));
                                    InscriptionDao.Type = "Medecin";
                                    new InscriptionDao().inscriptionM(F);
                                } else if (fan.isSelected()) {

                                    Fan F = new Fan();
                                    F.setNom(nom.getText());
                                    F.setPrenom(prenom.getText());
                                    F.setCompte(c);
                                    InscriptionDao.Type = "Fan";
                                    F.setCredit(0);
                                    try {
                                        F.setDateNaissance(sdf.parse(dae));
                                    } catch (ParseException ex) {                                        
                                    }
                                    F.setSexe(Sexe.getText());

                                    new InscriptionDao().inscriptionF(F);
                                } else if (joueu.isSelected()) {

                                    Joueur F = new Joueur();
                                    F.setNom(nom.getText());
                                    F.setPrenom(prenom.getText());
                                    F.setCompte(c);
                                    F.setCin(Integer.parseInt(cin.getText()));
                                    F.setClassement(1000);
                                    InscriptionDao.Type = "Joueur";
                                    F.setSexe(Sexe.getText());
                                    try {
                                        F.setDateNaissance(sdf.parse(dae));
                                    } catch (ParseException ex) {                                        
                                    }
                                    F.setNumTel(Integer.parseInt(Numerotelephone.getText()));
                                    new InscriptionDao().inscriptionJ(F);
                                } else if (arbitre.isSelected()) {

                                    Arbitre F = new Arbitre();
                                    F.setNom(nom.getText());
                                    F.setPrenom(prenom.getText());
                                    F.setCompte(c);
                                    F.setCin(00);
                                    try {
                                        F.setDateNaissance(sdf.parse(dae));
                                    } catch (ParseException ex) {                                        
                                    }
                                    F.setSexe(Sexe.getText());
                                    F.setNumTel(Integer.parseInt(Numerotelephone.getText()));
                                    InscriptionDao.Type = "Arbitre";
                                    new InscriptionDao().inscriptionA(F);
                                    

                                } else {

                                    ResponsableAntiDopage F = new ResponsableAntiDopage();
                                    F.setNom(nom.getText());
                                    F.setPrenom(prenom.getText());
                                    F.setCompte(c);
                                    F.setCin(00);
                                    InscriptionDao.Type = "Responsable";
                                    F.setSexe(Sexe.getText());
                                    F.setNumTel(Integer.parseInt(Numerotelephone.getText()));
                                    new InscriptionDao().inscriptionR(F);
                                    try {
                                        F.setDateNaissance(sdf.parse(dae));
                                    } catch (ParseException ex) {                                        
                                    }
                                }
                            }
                        } else {
                            Dialog.show("Erroor !! ", "Verify Your Pass", "ok", null);
                        }
                    }
                };
                connectionRequest2.setUrl("https://allwin.000webhostapp.com/usernameMembre.php");
                NetworkManager.getInstance().addToQueue(connectionRequest2);
               // SignIn.show();
            }
        });
        SignIn.setTitle("Welcome");
        SignIn.show();
    }

    public void inscriptionJ(Joueur membre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

                new Authentification().connect();

            }

        };
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        connectionRequest.setUrl("https://allwin.000webhostapp.com/inscription.php?nom=" + membre.getNom() + "&prenom=" + membre.getPrenom() + "&usernameMembre=" + membre.getCompte().getLogin() + "&passwordMembre=" + membre.getCompte().getPasseword() + "&emailMembre=" + membre.getCompte().getMail() + "&type=" + InscriptionDao.Type + "&cin=" + membre.getCin() + "&numTelMembre=" + membre.getNumTel() + "&sexeMembre=" + membre.getSexe() + "&dateNaissanceMembre=" + sdf.format(membre.getDateNaissance()));
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void inscriptionF(Fan membre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

                new Authentification().connect();

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/inscription.php?nom=" + membre.getNom() + "&prenom=" + membre.getPrenom() + "&usernameMembre=" + membre.getCompte().getLogin() + "&passwordMembre=" + membre.getCompte().getPasseword() + "&emailMembre=" + membre.getCompte().getMail() + "&type=" + InscriptionDao.Type + "&cin=" + 0 + "&numTelMembre=" + membre.getNumTel() + "&sexeMembre=" + membre.getSexe() + "&dateNaissanceMembre=" + membre.getDateNaissance());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void inscriptionM(Medecin membre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

                new Authentification().connect();

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/inscription.php?nom=" + membre.getNom() + "&prenom=" + membre.getPrenom() + "&usernameMembre=" + membre.getCompte().getLogin() + "&passwordMembre=" + membre.getCompte().getPasseword() + "&emailMembre=" + membre.getCompte().getMail() + "&type=" + InscriptionDao.Type + "&cin=" + membre.getCin() + "&numTelMembre=" + membre.getNumTel() + "&sexeMembre=" + membre.getSexe() + "&dateNaissanceMembre=" + membre.getDateNaissance());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void inscriptionA(Arbitre membre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

                new Authentification().connect();

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/inscription.php?nom=" + membre.getNom() + "&prenom=" + membre.getPrenom() + "&usernameMembre=" + membre.getCompte().getLogin() + "&passwordMembre=" + membre.getCompte().getPasseword() + "&emailMembre=" + membre.getCompte().getMail() + "&type=" + InscriptionDao.Type + "&cin=" + membre.getCin() + "&numTelMembre=" + membre.getNumTel() + "&sexeMembre=" + membre.getSexe() + "&dateNaissanceMembre=" + membre.getDateNaissance());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void inscriptionR(ResponsableAntiDopage membre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

                new Authentification().connect();

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/inscription.php?nom=" + membre.getNom() + "&prenom=" + membre.getPrenom() + "&usernameMembre=" + membre.getCompte().getLogin() + "&passwordMembre=" + membre.getCompte().getPasseword() + "&emailMembre=" + membre.getCompte().getMail() + "&type=" + InscriptionDao.Type + "&cin=" + membre.getCin() + "&numTelMembre=" + membre.getNumTel() + "&sexeMembre=" + membre.getSexe() + "&dateNaissanceMembre=" + membre.getDateNaissance());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
}
