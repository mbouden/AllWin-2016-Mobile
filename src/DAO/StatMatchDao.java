/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Joueur;
import Entity.Match;
import Entity.StatistiquesMatch;
import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.MyApplication;
import com.mycompany.myapp.MyWorld;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhia025
 */
public class StatMatchDao {

    Button d;
    public Joueur jj;
    Form list = new Form(new BoxLayout(BoxLayout.Y_AXIS));
    Form stats = new Form(new BoxLayout(BoxLayout.Y_AXIS));
    Form first = new Form(new BoxLayout(BoxLayout.Y_AXIS));

    public StatMatchDao() {

    }

    public void getStatMatch() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<StatistiquesMatch> statistiquesMatch = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    statistiquesMatch.clear();
                    for (Map<String, Object> obj : content) {

                        Match m = new Match();

                        statistiquesMatch.add(
                                new StatistiquesMatch(Integer.parseInt((String) obj.get("nbrAces")), Integer.parseInt((String) obj.get("nbrDoubleFaults")), Float.parseFloat((String) obj.get("pPremierService")), Integer.parseInt((String) obj.get("ptsDeuxiemeService")), Float.parseFloat((String) obj.get("balleBreak")), Integer.parseInt((String) obj.get("ptsSurRetour")), Integer.parseInt((String) obj.get("totalPtsGagnes")), m)
                        );

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {

                list.show();

            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/statMatch.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void addStatMatch(StatistiquesMatch sm) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {
                //  MyApplication.list.showBack();
                //  getStatMatch();
                System.out.println(sm.getIdMatch());
                System.out.println(sm.getIdMembre());
                System.out.println(sm.toString());
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/insertStatMatch.php?&idMatch=" + sm.getIdMatch() + "&idMembre=" + sm.getIdMembre() + "&nbrAces=" + sm.getNbrAces() + "&nbrDoubleFaults=" + sm.getNbrDoubleFaults() + "&pPremierService=" + sm.getpPremierService() + "&ptsPremierService=" + sm.getpPremierService() + "&ptsDeuxiemeService=" + sm.getPtsDeuxiemeService() + "&balleBreak=" + sm.getBalleBreak() + "&ptsSurRetour=" + sm.getPtsSurRetour() + "&totalPtsGagnes=" + sm.getTotalPtsGagnes());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    //****************get stat match
    public void getPieMatch() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<StatistiquesMatch> statMatch = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    statMatch.clear();
                    for (Map<String, Object> obj : content) {

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            statMatch.add(
                                    new StatistiquesMatch(Integer.parseInt((String) obj.get("nbrAces")), Integer.parseInt((String) obj.get("nbrDoubleFaults")), Float.parseFloat((String) obj.get("pPremierService")), Integer.parseInt((String) obj.get("ptsDeuxiemeService")), Integer.parseInt((String) obj.get("ptsDeuxiemeService")), Float.parseFloat((String) obj.get("balleBreak")), Integer.parseInt((String) obj.get("ptsSurRetour")), Integer.parseInt((String) obj.get("totalPtsGagnes")), (String) obj.get("nom_membre"), (String) obj.get("prenom_membre"), df.parse((String) obj.get("dateMatch")))
                            );
                        } catch (ParseException ex) {
                        }

                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                list.removeAll();
                list.setBackCommand(new Command(""){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        new MyWorld();
                    }
                });
                
                int i = 0;
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Form s = new Form();
                for (StatistiquesMatch cr : statMatch) {

                    d = new Button("" + cr.getNom() + " " + cr.getPrenom() + " " + cr.getDate());

                    stats.add(d);
                    double[] values = new double[]{cr.getBalleBreak(), cr.getNbrAces(), cr.getNbrDoubleFaults(), cr.getPtsPremierService(), cr.getPtsDeuxiemeService()};

                    d.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            Form x = new Form();
                            x = createPieChartForm(values);
                            
                            Command backCommand = new Command("") {
                                @Override
                                public void actionPerformed(ActionEvent evt) {
                                    stats.show();
                                }
                            };
                            x.getToolbar().setBackCommand(backCommand);
                            x.show();
                        }
                    });
                    stats.getToolbar().addCommandToSideMenu(new Command("MyWorld"){
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            new MyWorld();
                        }
                    });
                    stats.show();
                }

            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/getStatMatch.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    //*************************
    //*******************stats
    private DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 100});
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    /**
     * Builds a category series using the provided values.
     *
     * @param titles the series titles
     * @param values the values
     * @return the category series
     */
    protected CategorySeries buildCategoryDataset(String title, double[] values) {
        CategorySeries series = new CategorySeries(title);
        int k = 0;
        for (double value : values) {
            if (k == 0) {
                series.add("Balle Break", value);
            }
            if (k == 1) {
                series.add("Nbr Aces", value);
            }
            if (k == 2) {
                series.add("Nbr Double Faults", value);
            } //cr.getPtsPremierService(),cr.getPtsDeuxiemeService()
            if (k == 3) {
                series.add("Pts 1 service", value);
            }
            if (k == 4) {
                series.add("Pts 2 service", value);
            }

            k++;
        }

        return series;
    }

    public Form createPieChartForm(double[] values) {
        // Generate the values

        // Set up the renderer
        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA, ColorUtil.YELLOW, ColorUtil.CYAN};
        DefaultRenderer renderer = buildCategoryRenderer(colors);
        renderer.setZoomButtonsVisible(true);
        renderer.setZoomEnabled(true);
        renderer.setChartTitleTextSize(20);
        renderer.setDisplayValues(true);
        renderer.setShowLabels(true);
        SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);
        r.setGradientEnabled(true);
        r.setGradientStart(0, ColorUtil.BLUE);
        r.setGradientStop(0, ColorUtil.GREEN);
        r.setHighlighted(true);

        // Create the chart ... pass the values and renderer to the chart object.
        PieChart chart = new PieChart(buildCategoryDataset("Statistic ", values), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        // Create a form and show it.
        Form fx = new Form("Stat Match", new BorderLayout());
        fx.add(BorderLayout.CENTER, c);
        return fx;

    }

//********************** list Match*********************
    public void getListeMatch() {
        ComboBox mtch = new ComboBox();

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<StatistiquesMatch> statMatch = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    statMatch.clear();
                    for (Map<String, Object> obj : content) {

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        statMatch.add(
                                new StatistiquesMatch(Integer.parseInt((String) obj.get("idMatch")))
                        );

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {
                list.removeAll();

                for (StatistiquesMatch cr : statMatch) {
                    //  System.out.println("jjjjjjjjj"+cr.getIdMatch());
                    mtch.addItem(cr.getIdMatch());

                }

            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/getMatch.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

//*****************************
    public void getListJoueur() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<Joueur> joueur = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    joueur.clear();
                    for (Map<String, Object> obj : content) {

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        joueur.add(new Joueur(Integer.parseInt((String) obj.get("id"))));

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {
                list.removeAll();
                for (Joueur cr : joueur) {
                    MyApplication.cb.addItem(cr.getId());
                    MyApplication.cbb.addItem(cr.getId());
                    MyApplication.cbbb.addItem(cr.getId());
                }
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/getJoueur.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

//*****************************
}
