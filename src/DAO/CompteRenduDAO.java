/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Club;
import Entity.Compte;
import Entity.CompteRendu;
import Entity.Joueur;
import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.table.TableLayout;
import com.mycompany.myapp.MyApplication;
import com.mycompany.myapp.MyWorld;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
//import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dhia025
 */
public class CompteRenduDAO {

    Form list = new Form(new BoxLayout(BoxLayout.Y_AXIS));
    Form stats = new Form(new BoxLayout(BoxLayout.Y_AXIS));
    Form first = new Form(new BoxLayout(BoxLayout.Y_AXIS));
    ComboBox cb;
    int idj;
    public int j;
    public Joueur jj;
    String s;
    Form fs;
    Button d;

    public CompteRenduDAO() {
        stats.getToolbar().addCommandToSideMenu(new Command("List") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                getCompteRendu();
            }
        });

        stats.getToolbar().addCommandToSideMenu(new Command("Add") {
            @Override
            public void actionPerformed(ActionEvent evt) {
                addCompteRendu();
            }
        });
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {
        }

        Command myWorld = new Command("My World", home);
        list.getToolbar().addCommandToSideMenu(myWorld);
        list.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == myWorld) {
                    new MyWorld().show();
                }
            }
        });
    }

    public void getCompteRendu() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<CompteRendu> compteRendu = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    compteRendu.clear();
                    for (Map<String, Object> obj : content) {
                        Compte c = new Compte((String) obj.get("emailMembre"), (String) obj.get("passwordMembre"), (String) obj.get("usernameMembre"));
                        Club cl = new Club();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            /* try {
                            //jj = new Joueur(Integer.parseInt((String) obj.get("cinMembre")),Integer.parseInt((String) obj.get("numLicenceMembre")),Integer.parseInt((String) obj.get("classement")), (Date) obj.get("dateCreationLicence"),cl,Integer.parseInt((String) obj.get("score")),(String) obj.get("pays"),(String) obj.get("nomMembre"),(String)obj.get("prenomMembre"), (Date) obj.get("dateNaissanceMembre"),(String)obj.get("sexeMembre"),c,Integer.parseInt((String) obj.get("numTelMembre")) );
                            jj = new Joueur(Integer.parseInt((String) obj.get("cinMembre")),Integer.parseInt((String) obj.get("numLicenceMembre")),cl,Integer.parseInt((String) obj.get("score")),(String) obj.get("pays"),c,(String) obj.get("nomMembre"),(String) obj.get("prenomMembre"),df.parse((String) obj.get("dateNaissance")));
                            } catch (ParseException ex) {
                            }*/
                            jj = new Joueur(Integer.parseInt((String) obj.get("cin_membre")), Integer.parseInt((String) obj.get("Classement")), cl, Integer.parseInt((String) obj.get("score")), (String) obj.get("pays"), (String) obj.get("nom_membre"), (String) obj.get("prenom_membre"), df.parse((String) obj.get("date_naissance_membre")), (String) obj.get("sexe_membre"), c, Integer.parseInt((String) obj.get("num_tel_membre")));
                        } catch (ParseException ex) {
                        }
                        // 
                        try {
                            compteRendu.add(
                                    new CompteRendu(jj, Integer.parseInt((String) obj.get("echantillon")), Integer.parseInt((String) obj.get("ph")), Float.parseFloat((String) obj.get("densite")), Float.parseFloat((String) obj.get("volume")), df.parse((String) obj.get("dateCompteRendu")), Integer.parseInt((String) obj.get("idJoueur")))
                            );
                        } catch (ParseException ex) {
                        }

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {
                list.removeAll();
                for (CompteRendu cr : compteRendu) {
                    Container hi = new Container(new TableLayout(2, 2));
                    hi.add(new Label("Date")).
                            add(new Label("" + cr.getDate())).
                            add(new Label("Ph")).
                            add(new Label("" + cr.getPh())).
                            add(new Label("Densite")).
                            add(new Label("" + cr.getDensite())).
                            add(new Label("Volume")).
                            add(new Label("Volume " + cr.getVolume()));
                    hi.setUIID(".LoginButton");
                    list.add(hi);
                }
                list.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/compteRendu.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void addCompteRendu() {
        Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
        
        MyApplication.cbb = new ComboBox();
        MyApplication.cb = new ComboBox();
        MyApplication.cbb = new ComboBox();
        MyApplication.cbbb = new ComboBox();
        new StatMatchDao().getListJoueur();

        f.add(MyApplication.cbb);
        TextField ech = new TextField("", "Echantillon");
        TextField ph = new TextField("", "Ph");
        TextField d = new TextField("", "Density");
        TextField v = new TextField("", "Volume");
        DateSpinner date = new DateSpinner();

        f.add(ech);
        f.add(ph);
        f.add(d);
        f.add(v);
        f.add(date);

        Button bt = new Button("Validate");
        f.add(bt);
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                CompteRendu cr = new CompteRendu();
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                String dae = date.getCurrentYear() + "-" + date.getCurrentMonth() + "-" + date.getCurrentDay();
                try {
                    cr.setDate(dateFormatter.parse(dae));
                } catch (ParseException ex) {
                }
                cr.setDensite(Float.parseFloat(d.getText()));
                cr.setEchantillon(Integer.parseInt(ech.getText()));
                cr.setIdJoueur((Integer) MyApplication.cbb.getSelectedItem());
                cr.setPh(Integer.parseInt(ph.getText()));
                cr.setVolume(Float.parseFloat(v.getText()));
                ConnectionRequest connectionRequest;
                connectionRequest = new ConnectionRequest() {
                    @Override
                    protected void readResponse(InputStream in) throws IOException {
                        System.out.println(in);
                    }
                    @Override
                    protected void postResponse() {
                        stats.showBack();
                    }
                };

                connectionRequest.setUrl("https://allwin.000webhostapp.com/insert.php?&joueur=" + cr.getIdJoueur() + "&ech=" + cr.getEchantillon() + "&ph=" + cr.getPh() + "&densite=" + cr.getDensite() + "&volume=" + cr.getVolume() + "&date=" + dae);
                NetworkManager.getInstance().addToQueue(connectionRequest);
            }
        });
        f.getToolbar().setBackCommand(new Command(""){
            @Override
            public void actionPerformed(ActionEvent evt) {
                getCompteRendu();
            }
        });
        f.show();
    }

    public Form creatContainer(Form s, Button d) {
        // getCompteRendu();
        d.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                System.out.println("");

            }
        });

        stats.add(s);
        return stats;

    }

    //***************************************
    public void getstatCompteRendu() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<CompteRendu> compteRendu = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    compteRendu.clear();
                    for (Map<String, Object> obj : content) {
                        Compte c = new Compte((String) obj.get("emailMembre"), (String) obj.get("passwordMembre"), (String) obj.get("usernameMembre"));
                        Club cl = new Club();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        try {

                            jj = new Joueur(Integer.parseInt((String) obj.get("cin_membre")), Integer.parseInt((String) obj.get("Classement")), cl, Integer.parseInt((String) obj.get("score")), (String) obj.get("pays"), (String) obj.get("nom_membre"), (String) obj.get("prenom_membre"), df.parse((String) obj.get("date_naissance_membre")), (String) obj.get("sexe_membre"), c, Integer.parseInt((String) obj.get("num_tel_membre")));
                        } catch (ParseException ex) {
                        }

                        try {
                            compteRendu.add(
                                    new CompteRendu(jj, Integer.parseInt((String) obj.get("echantillon")), Integer.parseInt((String) obj.get("ph")), Float.parseFloat((String) obj.get("densite")), Float.parseFloat((String) obj.get("volume")), df.parse((String) obj.get("dateCompteRendu")), Integer.parseInt((String) obj.get("idJoueur")))
                            );
                        } catch (ParseException ex) {
                        }

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {
                list.removeAll();
                int i = 0;

                // fs= new Form();
                for (CompteRendu cr : compteRendu) {

                    d = new Button(cr.getJoueur().getNom() + " " + cr.getJoueur().getPrenom());
                    Form s = new Form(new BoxLayout(BoxLayout.Y_AXIS));
                    stats.add(d);
                    double[] values = new double[]{cr.getDensite(), cr.getPh(), cr.getVolume()};

                    d.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            Form x = new Form();
                            x = createPieChartForm(values);
                            Command backCommand = new Command("") {
                                @Override
                                public void actionPerformed(ActionEvent evt) {
                                    new CompteRenduDAO().getstatCompteRendu();
                                }
                            };
                            x.getToolbar().setBackCommand(backCommand);
                            x.show();
                        }
                    });
                }
                Image home = null;
                try {
                    home = Image.createImage("/tennis-court.png");
                } catch (IOException ex) {
                }

                Command myWorld = new Command("My World", home);
                stats.getToolbar().addCommandToSideMenu(myWorld);
                stats.addCommandListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (evt.getCommand() == myWorld) {
                            new MyWorld().show();
                        }
                    }
                });
                stats.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/compteRendu.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    //*******************stats
    private DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 100});
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    /**
     * Builds a category series using the provided values.
     *
     * @param titles the series titles
     * @param values the values
     * @return the category series
     */
    protected CategorySeries buildCategoryDataset(String title, double[] values) {
        CategorySeries series = new CategorySeries(title);
        int k = 0;
        for (double value : values) {
            if (k == 0) {
                series.add("Densite", value);
            }
            if (k == 1) {
                series.add("Ph", value);
            }
            if (k == 2) {
                series.add("Volume", value);
            }
            k++;
        }

        return series;
    }

    public Form createPieChartForm(double[] values) {
        // Generate the values

        // Set up the renderer
        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA, ColorUtil.YELLOW, ColorUtil.CYAN};
        DefaultRenderer renderer = buildCategoryRenderer(colors);
        renderer.setZoomButtonsVisible(true);
        renderer.setZoomEnabled(true);
        renderer.setChartTitleTextSize(20);
        renderer.setDisplayValues(true);
        renderer.setShowLabels(true);
        SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);
        r.setGradientEnabled(true);
        r.setGradientStart(0, ColorUtil.BLUE);
        r.setGradientStop(0, ColorUtil.GREEN);
        r.setHighlighted(true);

        // Create the chart ... pass the values and renderer to the chart object.
        PieChart chart = new PieChart(buildCategoryDataset("Statistic ", values), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        // Create a form and show it.
        Form fx = new Form("Stat Joueur", new BorderLayout());
        fx.add(BorderLayout.CENTER, c);
        return fx;

    }

    //**************************
    public void getListJoueur() {

        ConnectionRequest connectionRequest;

        connectionRequest = new ConnectionRequest() {
            List<CompteRendu> compteRendu = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    compteRendu.clear();
                    for (Map<String, Object> obj : content) {
                        Compte c = new Compte((String) obj.get("emailMembre"), (String) obj.get("passwordMembre"), (String) obj.get("usernameMembre"));
                        Club cl = new Club();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        try {

                            jj = new Joueur(Integer.parseInt((String) obj.get("cin_membre")), Integer.parseInt((String) obj.get("Classement")), cl, Integer.parseInt((String) obj.get("score")), (String) obj.get("pays"), (String) obj.get("nom_membre"), (String) obj.get("prenom_membre"), df.parse((String) obj.get("date_naissance_membre")), (String) obj.get("sexe_membre"), c, Integer.parseInt((String) obj.get("num_tel_membre")));
                        } catch (ParseException ex) {
                        }

                        try {
                            compteRendu.add(
                                    new CompteRendu(jj, Integer.parseInt((String) obj.get("echantillon")), Integer.parseInt((String) obj.get("ph")), Float.parseFloat((String) obj.get("densite")), Float.parseFloat((String) obj.get("volume")), df.parse((String) obj.get("dateCompteRendu")), Integer.parseInt((String) obj.get("idJoueur")))
                            );
                        } catch (ParseException ex) {
                        }

                    }
                } catch (IOException err) {
                    Log.e(err);
                }

            }

            @Override
            protected void postResponse() {

                int i = 0;
                cb = new ComboBox();
                for (CompteRendu cr : compteRendu) {
                    cb.addItem(cr.getIdJoueur());

                }

            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/compteRendu.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

}
