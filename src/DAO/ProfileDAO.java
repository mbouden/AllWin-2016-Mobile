/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.MyWorld;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author bilel
 */
public class ProfileDAO {

    

    public void getProfile() {

        Container cont = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label nom = new Label("Nom: " + Authentification.getM().getNom());
        Label prenom = new Label("Prenom: " + Authentification.getM().getPrenom());
        Label mail = new Label("Mail: " + Authentification.getM().getCompte().getMail());
        Label username = new Label("Username: " + Authentification.getM().getCompte().getLogin());
        Label sexe = new Label("Sexe: " + Authentification.getM().getSexe());
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
        String dateTime3 = dateParser.format( Authentification.getM().getDateNaissance());
        Label date = new Label("Date de Naissance: " +dateTime3);
        Label tel = new Label(String.valueOf("Numéro Telephone: " + Authentification.getM().getNumTel()));
        Button update = new Button("Update");

        cont.add(nom);
        cont.add(prenom);
        cont.add(username);
        cont.add(mail);
        cont.add(sexe);
        cont.add(date);
        cont.add(tel);
        cont.add(update);
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
                TextField tmail = new TextField("", "Mail");
                TextField tusername = new TextField("", "Username");
                TextField tnum = new TextField("", "Phone Number");
                Button confirm = new Button("Confirm");
                f.add(tmail);
                f.add(tusername);
                f.add(tnum);
                f.add(confirm);
                f.setBackCommand(new Command(""){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        new MyWorld();
                    }
                });
                confirm.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (!tmail.getText().trim().equals("")) {
                            Authentification.getM().getCompte().setMail(tmail.getText().trim());
                        }
                        if (!tusername.getText().trim().equals("")) {
                            Authentification.getM().getCompte().setLogin(tusername.getText().trim());
                        }
                        if (!tnum.getText().trim().equals("")) {
                            Authentification.getM().setNumTel(Integer.parseInt(tnum.getText().trim()));
                        }
                        ConnectionRequest connectionRequest;
                        connectionRequest = new ConnectionRequest() {
                            @Override
                            protected void readResponse(InputStream in) throws IOException {
                                System.out.println(in);

                            }

                            @Override
                            protected void postResponse() {
                                  new ProfileDAO().getProfile();
                            }
                        };
                        connectionRequest.setUrl("http://allwin.000webhostapp.com/update.php?id=" + Authentification.getM().getId() + "&mail=" + Authentification.getM().getCompte().getMail() + "&username=" + Authentification.getM().getCompte().getLogin() + "&numtel=" + Authentification.getM().getNumTel());
                        NetworkManager.getInstance().addToQueue(connectionRequest);
                        
                    }
                });
                f.show();
            }
        });
        
        Form profile = new Form();
        Command cmd = new Command("");
                
                profile.getToolbar().setBackCommand(cmd);
                        profile.addCommandListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent evt) {
                                if (evt.getCommand() == cmd) {
                                    new MyWorld();
                                }
                            }
                        });
        profile.add(cont);
        profile.show();
    }
}
