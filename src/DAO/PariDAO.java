/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Fan;
import Entity.Joueur;
import Entity.Match;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
//import com.codename1.startapp.StartAppManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
//import static com.mycompany.myapp.MyApplication.manager;
import com.mycompany.myapp.PariForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mahmoud
 */
public class PariDAO {

    static int idFeuille;

    public void afficher() {
        //manager.loadAd(StartAppManager.AD_INTERSTITIALS);
        //manager.showAd();
        ConnectionRequest cr;
        cr = new ConnectionRequest() {
            List<Match> listMatchs = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    for (Map<String, Object> obj : content) {
                        try {
                            PariDAO.idFeuille = Integer.parseInt((String) obj.get("idFeuillePari"));
                            for (int i = 1; i < 13; i++) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String d = (String) obj.get("dateMatch" + i);
                                sdf.format(d);
                                listMatchs.add(new Match((String) obj.get("lieuMatch" + i),
                                        sdf.parse(d),
                                        new Date(), new Date(), 50,
                                        new Arbitre("mahmoud", "bouden")));
                            }
                            for (int i = 0; i < 12; i++) {
                                listMatchs.get(i).setIdMatch(Integer.parseInt((String) obj.get("idMatch" + (i + 1))));
                                Joueur j1 = new Joueur();
                                Joueur j2 = new Joueur();
                                j1.setNom((String) obj.get("nomJoueur1" + (i + 1)));
                                j1.setPrenom((String) obj.get("prenomJoueur1" + (i + 1)));

                                j2.setNom((String) obj.get("nomJoueur2" + (i + 1)));
                                j2.setPrenom((String) obj.get("prenomJoueur2" + (i + 1)));
                                List<Joueur> listJoueurs = new ArrayList<>();
                                listJoueurs.add(j1);
                                listJoueurs.add(j2);
                                listMatchs.get(i).setJoueurs(listJoueurs);
                            }
                        } catch (ParseException ex) {
                        }
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                PariForm f = new PariForm();
                List<RadioButton> listRadio = new ArrayList<>();
                int i = 1;
                for (Match m : listMatchs) {
                    String txt = "";
                    txt = listMatchs.get(i - 1).getJoueurs().get(0).toString() + " VS " + listMatchs.get(i - 1).getJoueurs().get(1).toString();
                    Label lb = new Label(i + "- " + txt);
                    lb.setUIID("CheckBox");
                    Container ct = new Container(new BoxLayout(BoxLayout.X_AXIS));
                    Container ct2 = BorderLayout.west(lb);
                    ct2.setUIID("CheckBox");
                    RadioButton rd1 = new RadioButton("1");
                    RadioButton rdJ = new RadioButton("J");
                    RadioButton rd2 = new RadioButton("2");
                    rdJ.setUIID("RBJ");
                    String groupName = String.valueOf(i);
                    rd1.setGroup(groupName);
                    rd2.setGroup(groupName);
                    rdJ.setGroup(groupName);
                    listRadio.add(rd1);
                    listRadio.add(rdJ);
                    listRadio.add(rd2);
                    ct.add(rd1);
                    ct.add(rdJ);
                    ct.add(rd2);
                    ct2.add(BorderLayout.EAST, ct);
                    ct2.getStyle().setOpacity(150);
                    f.add(ct2);
                    i++;
                }
                Button confirm = new Button("Confirm");
                confirm.addActionListener(new ActionListener() {
                    String choix;

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        boolean flag = true;
                        int nbrJokers = 0;
                        int prix = 5;
                        choix = "";
                        for (int i = 0; i < 36; i += 3) {
                            if (listRadio.get(i).isSelected()) {
                                choix += "1";
                            } else if (listRadio.get(i + 1).isSelected()) {
                                choix += "J";
                                nbrJokers++;
                                prix *= 2;
                            } else if (listRadio.get(i + 2).isSelected()) {
                                choix += "2";
                            } else if (Dialog.show("Error", "Match Number " + listRadio.get(i).getGroup() + " Is Not Selected !!", "OK", null)) {
                                new PariDAO().afficher();
                                flag = false;
                                break;
                            }
                        }
                        if (nbrJokers > 6) {
                            if (Dialog.show("Error", "Maximum Number Of Jokers Reached !!", "Yes", null)) {
                                new PariDAO().afficher();
                                flag = false;
                            }
                        }
                        Fan fan  = (Fan) Authentification.getM();
                        if (fan.getCredit()<prix) {
                            if (Dialog.show("Error !", "Sorry, You dont have enough money !", "OK !", null)) {
                                new PariDAO().afficher();
                                flag = false;
                            }
                        }
                        ConnectionRequest cr;
                        cr = new ConnectionRequest() {
                            @Override
                            protected void readResponse(InputStream input) throws IOException {

                            }

                            @Override
                            protected void postResponse() {

                            }
                        };
                        if (flag) {
                            if (Dialog.show("Confirm", "This will cost " + prix + " DT. Are You Sure You Want To Procceed ?", "Yes", "No")) {

                                System.out.println(PariDAO.idFeuille + " " + choix);
                                cr.setUrl("https://allwin.000webhostapp.com/EnvoyerChoix.php?idFeuille="
                                        + PariDAO.idFeuille + "&prix=" + prix + "&nbrJokers=" + nbrJokers + "&idMembre=" + Authentification.getM().getId() + "&choix=" + choix);
                                NetworkManager.getInstance().addToQueue(cr);
                                if (Dialog.show("Successfull", "Your Bet Has Been Saved, Good Luck !", "OK", null)) {
                                    
                                    fan.setCredit(fan.getCredit()-prix);
                                    ConnectionRequest cr2;
                                    cr2 = new ConnectionRequest() {
                                        @Override
                                        protected void readResponse(InputStream input) throws IOException {

                                        }

                                        @Override
                                        protected void postResponse() {

                                        }
                                    };
                                    cr2.setUrl("https://allwin.000webhostapp.com/PrixPari.php?id="+fan.getId()+"&credit="+fan.getCredit());
                                    NetworkManager.getInstance().addToQueue(cr2);
                                    new PariDAO().afficher();
                                }
                            }
                        }
                    }
                });
                f.add(confirm);
                f.show();
            }
        };

        cr.setUrl("https://allwin.000webhostapp.com/AfficherListPari.php");
        NetworkManager.getInstance().addToQueue(cr);
    }

    public static void loadAd() {
        //admob.loadAndShow();
    }
}
