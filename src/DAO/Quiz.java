/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.RadioButton;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.FlowLayout;
import com.mycompany.myapp.QuizForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mahmoud
 */
public class Quiz {

    List<String> listQuestion;
    List<String> listAnswers;
    List<String> listCorrectAnswers;

    public void getQuiz() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream input) throws IOException {
                listQuestion = new ArrayList<>();
                listAnswers = new ArrayList<>();
                listCorrectAnswers = new ArrayList<>();
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, String>> content
                            = (List<Map<String, String>>) data.get("root");

                    for (Map<String, String> obj : content) {
                        listQuestion.add((String) obj.get("question"));
                        listAnswers.add((String) obj.get("reponse1"));
                        listAnswers.add((String) obj.get("reponse2"));
                        listAnswers.add((String) obj.get("reponse3"));
                        listCorrectAnswers.add((String) obj.get("correct"));
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                QuizForm qf = new QuizForm();
                List<RadioButton> listRadio = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    SpanLabel ln = new SpanLabel(i + 1 + "- " + listQuestion.get(i));
                    ln.getStyle().setOpacity(200);
                    qf.add(ln);
                    for (int j = i * 3; j < (i + 1) * 3; j += 3) {
                        Container ct = new Container(new FlowLayout());
                        RadioButton rd = new RadioButton(listAnswers.get(j));
                        RadioButton rd1 = new RadioButton(listAnswers.get(j + 1));
                        RadioButton rd2 = new RadioButton(listAnswers.get(j + 2));
                        rd.getStyle().setOpacity(200);
                        rd1.getStyle().setOpacity(200);
                        rd2.getStyle().setOpacity(200);
                        rd.setGroup(String.valueOf(i));
                        rd1.setGroup(String.valueOf(i));
                        rd2.setGroup(String.valueOf(i));
                        listRadio.add(rd);
                        listRadio.add(rd1);
                        listRadio.add(rd2);
                        ct.add(rd).add(rd1).add(rd2);
                        
                        qf.add(ct);
                    }
                }
                Button btn = new Button("Submit");
                
                btn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        int score = 0;
                        int j = 0;
                        for (int i = 0; i < 15; i += 3) {
                            String choix1 = "";
                            String choix2 = "";
                            String choix3 = "";
                            if (listRadio.get(i).isSelected()) {
                                choix1 = listAnswers.get(i);
                            } else if (listRadio.get(i + 1).isSelected()) {
                                choix2 = listAnswers.get(i + 1);
                            } else if (listRadio.get(i + 2).isSelected()) {
                                choix3 = listAnswers.get(i + 2);
                            }

                            if (!"".equals(choix1)) {
                                if (choix1.equals(listCorrectAnswers.get(j))) {
                                    score++;
                                    listRadio.get(i).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i + 1).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i + 1).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i + 2).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i + 2).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                }
                            }

                            if (!"".equals(choix2)) {
                                if (choix2.equals(listCorrectAnswers.get(j))) {
                                    score++;
                                    listRadio.get(i + 1).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i + 2).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i + 2).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                }
                            }

                            if (!"".equals(choix3)) {
                                if (choix3.equals(listCorrectAnswers.get(j))) {
                                    score++;
                                    listRadio.get(i + 2).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i + 1).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i + 1).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i).getStyle().setFgColor(0xff0000, true);  //red
                                } else if (listRadio.get(i).getText().equals(listCorrectAnswers.get(j))) {
                                    listRadio.get(i).getStyle().setFgColor(0x00ff00, true);  //green
                                    listRadio.get(i + 2).getStyle().setFgColor(0xff0000, true);  //red
                                    listRadio.get(i + 1).getStyle().setFgColor(0xff0000, true);  //red
                                }
                            }

                            j++;
                        }
                        if (Dialog.show("Quiz Completed !", "Your Score is: " + score + "/5", "OK!", "Try Again?")) {
                        } else {
                            new Quiz().getQuiz();
                        }
                    }
                });
                qf.add(btn);
                qf.show();
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/getQuiz.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
