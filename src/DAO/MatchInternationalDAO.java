/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Joueur;
import com.codename1.components.MultiButton;
import Entity.MatchInternational;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.mycompany.myapp.MatchInternationalForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bilel
 */
public class MatchInternationalDAO {
//    Form list;
//    Toolbar T;
//    Command cmd = new Command("Go Back");
//    Button bt= new Button("Go Back");;
    public void getMatchInternational() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<MatchInternational> lmi = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");
                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> listeMatchInternational = (List<Map<String, Object>>) data.get("root");
                    lmi.clear();
                    for (Map<String, Object> obj : listeMatchInternational) {
                        List<Joueur> lj = new ArrayList<Joueur>();
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

                        String dateTime2 = (String) obj.get("date_naissance_membre1");
                        Date date2 = dateParser.parse(dateTime2);
                        lj.add(new Joueur((String) obj.get("nom_membre1"), (String) obj.get("prenom_membre1"), date2, (String) obj.get("sexe_membre1"), Integer.parseInt((String) obj.get("num_tel_membre1"))));
                        
                        String dateTime3 = (String) obj.get("date_naissance_membre2");
                        Date date3 = dateParser.parse(dateTime2);
                        lj.add(new Joueur((String) obj.get("nom_membre2"), (String) obj.get("prenom_membre2"), date3, (String) obj.get("sexe_membre2"), Integer.parseInt((String) obj.get("num_tel_membre2"))));

                        String dateTime = (String) obj.get("dateMatch");
                        Date date = dateParser.parse(dateTime);

                        String dateTime4 = (String) obj.get("date_naissance_membre2");
                        Date date4 = dateParser.parse(dateTime4);
                        MatchInternational mi = new MatchInternational((String) obj.get("evenement"), (String) obj.get("lieuMatch"), date, null, null, 0, new Arbitre((String) obj.get("nom_membreA"), (String) obj.get("prenom_membreA"), date4, (String) obj.get("sexe_membreA"),Integer.parseInt((String) obj.get("num_tel_membreA"))));
                        mi.setJoueurs(lj);
                        mi.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));
                        lmi.add(mi);
                    }
                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }
            @Override
            protected void postResponse() {
                MatchInternationalForm f = new MatchInternationalForm();
                f.removeAll();
                for (MatchInternational mi : lmi) {
                    MultiButton mb = new MultiButton();
                    mb.setUIID("MultiButton");
                    mb.setTextLine4("Lieu Match " + mi.getLieu());
                    mb.setTextLine2("Date Match " + mi.getDateMatch());
                    mb.setTextLine3("" + mi.getJoueurs().get(0).getNom()+" "+mi.getJoueurs().get(0).getPrenom()+"             VS                  " + mi.getJoueurs().get(1).getNom()+" "+mi.getJoueurs().get(1).getPrenom() );
                    mb.setTextLine1("Evenement  " + mi.getEvenementInternational());
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(150);
                    f.add(mb);
                   
                }
            f.show();
            }   
        };
        connectionRequest.setUrl("http://allwin.000webhostapp.com/getMatchInternational.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
            }
}
