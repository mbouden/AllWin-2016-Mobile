/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Fan;
import Entity.Joueur;
import Entity.Match;
import Entity.Ticket;
import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;
import com.mycompany.myapp.TicketForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author WiKi
 */
public class TicketDAO {

    int ch = 0;
    StringBuffer str;

    public void getMatchs() {
        Form f = new TicketForm();
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Match> Matchs = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Matchs.clear();
                    for (Map<String, Object> obj : content) {
                        Match c = new Match();
                        c.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));
                        c.setDateMatch(sdf.parse((String) obj.get("dateMatch")));
                        c.setLieu((String) obj.get("lieuMatch"));
                        Joueur J = new Joueur();
                        J.setNom((String) obj.get("nomMembre1"));
                        J.setPrenom((String) obj.get("prenomMembre1"));
                        Joueur h = new Joueur();
                        h.setNom((String) obj.get("nomMembre"));
                        h.setPrenom((String) obj.get("prenomMembre"));
                        List<Joueur> joueurs = new ArrayList<>();
                        joueurs.add(J);
                        joueurs.add(h);
                        c.setJoueurs(joueurs);
                        Matchs.add(c);
                        System.out.println(c);
                    }

                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }

            @Override
            protected void postResponse() {
                
                f.removeAll();
                for (Match M : Matchs) {
                    MultiButton mb = new MultiButton(M.getJoueurs().get(0).getNom() + " " + M.getJoueurs().get(0).getPrenom() + " vs " + M.getJoueurs().get(1).getNom() + " " + M.getJoueurs().get(1).getPrenom());

                    mb.setTextLine2("#" + M.getDateMatch());
                    mb.setTextLine3("#" + M.getLieu());

                    f.add(mb);
                    mb.addActionListener(e -> {
                        ByTicket(M.getIdMatch(), Authentification.getM().getId());
                    });
                }
                f.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/AfficheMatchs.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

      
    public void AddTicket( Ticket T)
            
    { 
         ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {
                
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/insertTicket.php?prix="+T.getPrix()+"&Match=" +T.getMatch().getIdMatch()+"&membre="+T.getFan().getId()+"&chaise="+T.getChaise());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
    
    public void updateMatch(Match M)
     { 
         ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {
                
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/Updatenbr.php?id="+ M.getIdMatch() +"&nbrTicket="+M.getNbrTicket());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
    public void updateCredit(Fan F) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/updatecredit.php?id=" +Authentification.getM().getId() + "&credit=" + F.getCredit());
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void ByTicket(int id1, int id2) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Match> Ma = new ArrayList<>();
            float x = 0;
            Fan F = new Fan();
            Ticket T = new Ticket();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Ma.clear();
                    for (Map<String, Object> obj : content) {
                        Match c = new Match();

                        F.setId(id2);

                        c.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));
                        c.setNbrTicket(Integer.parseInt((String) obj.get("nbrTickets")));
                        x = Float.parseFloat((String) obj.get("creditMembre"));
                        F.setCredit(x);
                        Ma.add(c);
                        System.out.println(c);
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                if (Dialog.show("Achat", "the price of this Ticket is 50$ !! please confirm ", "Yes", "NO")) {
                    if (x < 50) { //Dialog Diag = new Dialog("Alert");
                        //Diag.show("error" , "your credit is insuffisant","yes","no"); 
                        Dialog d = new Dialog("error");
                        TextArea popupBody = new TextArea("sorry insuffisant credit", 3, 15);
                        popupBody.setUIID("PopupBody");
                        popupBody.setEditable(false);
                        d.setLayout(new BorderLayout());
                        d.add(BorderLayout.CENTER, popupBody);
                        final Button showPopup = new Button("Show Popup");
                        d.showPopupDialog(showPopup);

                    } else {
                        for (Match M : Ma) {
                            T.setPrix(50);
                            T.setFan(F);
                            T.setMatch(M);
                            Random r = new Random();
                            int valeur = 1 + r.nextInt(199);
                            T.setChaise(valeur);
                            AddTicket(T);
                            M.setNbrTicket(M.getNbrTicket() - 1);
                            F.setCredit(F.getCredit() - 50);
                            updateMatch(M);
                            updateCredit(F);

                            //Dialog.show("Confirmation", "hope you enjoy the match " +T.getChaise(), "Yes", "NO") ;
                            Dialog d = new Dialog("Confirmation");
                            TextArea popupBody = new TextArea("hope you enjoy the match Your seat's number is " + T.getChaise(), 3, 15);
                            popupBody.setUIID("PopupBody");
                            popupBody.setEditable(false);
                            d.setLayout(new BorderLayout());
                            d.add(BorderLayout.CENTER, popupBody);
                            final Button showPopup = new Button("Show Popup");
                            d.showPopupDialog(showPopup);

                        }
                    }
                }
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/achatTiquet.php?id1=" + id1 + "&id2=" + Authentification.getM().getId());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
