/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.LienStreaming;
import Entity.Match;
import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.mycompany.myapp.LienStreamingForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mahmoud
 */
public class LienStreamingDAO {

    public void findAll() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<LienStreaming> colors = new ArrayList<>();
            Match m;

            @Override
            protected void readResponse(InputStream in) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    colors.clear();
                    for (Map<String, Object> obj : content) {
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            String d = (String) obj.get("dateMatch");
                            sdf.format(d);
                            System.out.println(sdf.toPattern());
                            System.out.println(d);
                            m = new Match((String) obj.get("lieuMatch"),
                                    sdf.parse(d),
                                    new Date(), new Date(), 50,
                                    new Arbitre("Bouden", "Mahmoud"));
                        } catch (ParseException ex) {
                        }

                        LienStreaming ls = new LienStreaming(Integer.parseInt((String) obj.get("idLien")), (String) obj.get("urlLien"), m);
                        colors.add(ls);
                        System.out.println(ls);
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                LienStreamingForm f = new LienStreamingForm();
                f.removeAll();
                colors.toString();
                for (LienStreaming lien : colors) {
                    Image home = null;
                    try {
                        home = Image.createImage("/youtube.png");
                    } catch (IOException ex) {
                    }
                    
                    MultiButton mb = new MultiButton(lien.getMatch().toString());
                    mb.setTextLine3(lien.getMatch().getArbitre().toString());
                    mb.setTextLine2("Watch");
                    mb.getStyle().setOpacity(150);
                    mb.setIcon(home);
                    f.add(mb);
                    mb.addActionListener((ActionListener) (ActionEvent e) -> {
                        Display.getInstance().execute("https://www.youtube.com/watch?v=" + lien.getUrl());
                    });
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(160);
                }
                f.animateHierarchy(500);
                f.show();
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/allLienStreaming.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
}
