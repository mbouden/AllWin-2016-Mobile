/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author WiKi
 */
import Entity.Competition;
import Entity.Joueur;
import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.table.TableLayout;
import com.mycompany.myapp.CompetitionForm;
import com.mycompany.myapp.MyWorld;
import com.mycompany.myapp.PlayerInscriptionForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CompetitionDAO {

    public void getCompetitions() {
        Form f = new CompetitionForm();
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Competition> Competitions = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Competitions.clear();
                    for (Map<String, Object> obj : content) {
                        Competition c = new Competition((String) obj.get("nomCompetition"), sdf.parse((String) obj.get("dateDebutCompetition")), sdf.parse((String) obj.get("dateFinCompetition")), (String) obj.get("lieuCompetition"), Integer.parseInt((String) obj.get("nbrMaxJoueur")));
                        c.setIdCompetition(Integer.parseInt((String) obj.get("idCompetition")));
                        Competitions.add(c);
                        System.out.println(c);
                    }

                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }

            @Override
            protected void postResponse() {

                f.removeAll();

                for (Competition comp : Competitions) {
                    MultiButton mb = new MultiButton(comp.getNom());

                    mb.setTextLine2("#" + comp.getDate_debut());
                    mb.setTextLine3("#" + comp.getDate_fin());
                    mb.setTextLine4("#" + comp.getLieu());

                    f.add(mb);
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(150);
                    mb.addActionListener(e -> {
                        Form Options = new Form(new BoxLayout(BoxLayout.Y_AXIS));
                        Image home = null;
                        try {
                            home = Image.createImage("/tennis-court.png");
                        } catch (IOException ex) {
                        }

                        Command myWorld = new Command("My World", home);
                        Options.getToolbar().addCommandToSideMenu(myWorld);
                        Options.addCommandListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent evt) {
                                if (evt.getCommand() == myWorld) {
                                    new MyWorld().show();
                                }
                            }
                        });
                        Button bt = new Button("Show Players");
                        Button bt1 = new Button("List of Matches");
                        bt.addActionListener(k -> {
                            showPlayers(comp.getIdCompetition());
                        });
                        bt1.addActionListener(h -> {
                            if (comp.getNBR_MAX_JOUEURS() == 0) {
                                listmatch(comp.getIdCompetition());
                            } else {
                                //Dialog Diag = new Dialog("Alert");
                                //Diag.show("Table non reussi", "alaaaaaa", "yes", "no");
                                Dialog d = new Dialog("Sorry!!");
                                TextArea popupBody = new TextArea("Table not ready yet", 3, 15);
                                popupBody.setUIID("PopupBody");
                                popupBody.setEditable(false);
                                d.setLayout(new BorderLayout());
                                d.add(BorderLayout.CENTER, popupBody);
                                final Button showPopup = new Button("Show Popup");
                                d.showPopupDialog(showPopup);
                            }
                        });
                        Options.add(bt);
                        Options.add(bt1);
                        Options.show();
                    });
                }
                f.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/colors.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void showPlayers(int id) {
        Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {
        }

        Command myWorld = new Command("My World", home);
        f.getToolbar().addCommandToSideMenu(myWorld);
        f.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == myWorld) {
                    new MyWorld().show();
                }
            }
        });
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Joueur> Joueurs = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Joueurs.clear();
                    for (Map<String, Object> obj : content) {
                        Joueur c = new Joueur();
                        c.setId(Integer.parseInt((String) obj.get("idMembre")));
                        c.setNom((String) obj.get("nomMembre"));
                        c.setPrenom((String) obj.get("prenomMembre"));
                        Joueurs.add(c);
                        System.out.println(c);
                    }

                } catch (IOException err) {
                    Log.e(err);

                }

            }

            @Override
            protected void postResponse() {
                f.removeAll();
                for (Joueur J : Joueurs) {
                    MultiButton mb = new MultiButton(J.getNom() + " " + J.getPrenom());
                    f.add(mb);
                    mb.addActionListener(e -> {
                        showStat(J.getId());
                    });
                    mb.setUIID("RadioButton");
                }
                f.show();
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/joueurComp.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void showStat(int id) {
        Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {
        }

        Command myWorld = new Command("My World", home);
        f.getToolbar().addCommandToSideMenu(myWorld);
        f.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == myWorld) {
                    new MyWorld().show();
                }
            }
        });
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Joueur> statistiques = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    statistiques.clear();
                    for (Map<String, Object> obj : content) {
                        Joueur c = new Joueur();
                        c.setId(Integer.parseInt((String) obj.get("idMembre")));
                        c.setNom((String) obj.get("nomMembre"));
                        c.setDateNaissance(sdf.parse((String) obj.get("dateNaissanceMembre")));
                        c.setClassement(Integer.parseInt((String) obj.get("classement")));
                        c.setScore(Integer.parseInt((String) obj.get("score")));
                        c.setPays((String) obj.get("pays"));

                        statistiques.add(c);
                        System.out.println(c.getId());
                    }

                } catch (IOException err) {
                    Log.e(err);

                } catch (ParseException ex) {

                }

            }

            @Override
            protected void postResponse() {
                f.removeAll();
                for (Joueur J : statistiques) {
                    Form hi = new Form("" + J.getNom(), new TableLayout(4, 2));
                    hi.add(new Label("dateNaissance")).
                            add(new Label(J.getDateNaissance() + "")).
                            add(new Label("classement")).
                            add(new Label("" + J.getClassement())).
                            add(new Label("score")).
                            add(new Label("" + J.getScore())).
                            add(new Label("pays")).
                            add(new Label("" + J.getPays()));
                    f.add(hi);
                }
                f.show();
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/statJoueur.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void listmatch(int id) {
        Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {
        }

        Command myWorld = new Command("My World", home);
        f.getToolbar().addCommandToSideMenu(myWorld);
        f.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == myWorld) {
                    new MyWorld().show();
                }
            }
        });
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Joueur> matchs = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    matchs.clear();
                    for (Map<String, Object> obj : content) {
                        Joueur c = new Joueur();
                        c.setId(Integer.parseInt((String) obj.get("idMembre")));
                        c.setNom((String) obj.get("nomMembre"));
                        c.setPrenom((String) obj.get("prenomMembre"));
                        matchs.add(c);
                        System.out.println(c.getId());
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                int i = 0;
                String a = null;

                f.removeAll();
                for (Joueur J : matchs) {
                    i = i + 1;
                    if (i % 2 != 0) {
                        a = J.getNom() + " " + J.getPrenom() + " vs ";

                    } else {
                        Form hi = new Form("Match " + i / 2, new TableLayout(2, 2));
                        hi.add(new Label(a)).
                                add(new Label(J.getNom() + " " + J.getPrenom())).
                                add(new Label("Round")).
                                add(new Label("first round"));
                        f.add(hi);

                    }
                }
                f.show();
            }
        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/MatchCom.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void CompetitionsToInscription() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Competition> Competitions = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Competitions.clear();
                    for (Map<String, Object> obj : content) {
                        Competition c = new Competition((String) obj.get("nomCompetition"), sdf.parse((String) obj.get("dateDebutCompetition")), sdf.parse((String) obj.get("dateFinCompetition")), (String) obj.get("lieuCompetition"), Integer.parseInt((String) obj.get("nbrMaxJoueur")));
                        c.setIdCompetition(Integer.parseInt((String) obj.get("idCompetition")));
                        Competitions.add(c);
                        System.out.println(c);
                    }

                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }

            @Override
            protected void postResponse() {
                Form f = new PlayerInscriptionForm();

                for (Competition comp : Competitions) {
                    MultiButton mb = new MultiButton(comp.getNom());

                    mb.setTextLine2("#" + comp.getDate_debut());
                    mb.setTextLine3("#" + comp.getDate_fin());
                    mb.setTextLine4("#" + comp.getLieu());
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(150);
                    f.add(mb);
                    mb.addActionListener(e -> {
                        CompInscription(comp.getIdCompetition(), 4);

                    });
                }
                f.show();

            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/CompetitionToInscription.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void updateIdCompetition(int id, int id1) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/updateIdCompetition.php?id=" + id + "&id1=" + Authentification.getM().getId());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void updateNbrJoueur(int nbr, int id) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/updateNbrJoueurCom.php?nbr=" + nbr + "&id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void CompInscription(int id, int id1) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Competition> Comp = new ArrayList<>();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            // Date d ;
            //Date S = new Date();
            int x;

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Comp.clear();
                    for (Map<String, Object> obj : content) {
                        Competition C = new Competition();
                        C.setIdCompetition(Integer.parseInt((String) obj.get("idCompetition")));
                        C.setNBR_MAX_JOUEURS(Integer.parseInt((String) obj.get("nbrMaxJoueur")));
                        //d=sdf.parse((String) obj.get("dateFinCompetition1")) ;
                        x = (Integer.parseInt((String) obj.get("idCompetition1")));
                        Comp.add(C);

                        System.out.println(C);
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                if (Dialog.show("Inscription", "Are you sure of your Inscription ", "Yes", "NO")) {
                    for (Competition M : Comp) {
                        if (M.getNBR_MAX_JOUEURS() == 0) {
                            Dialog d = new Dialog("Inscription Failed");
                            TextArea popupBody = new TextArea("we are sorry but table of the Competition is already done", 3, 15);
                            popupBody.setUIID("PopupBody");
                            popupBody.setEditable(false);
                            d.setLayout(new BorderLayout());
                            d.add(BorderLayout.CENTER, popupBody);
                            final Button showPopup = new Button("Show Popup");
                            d.showPopupDialog(showPopup);

                        } else if (x == M.getIdCompetition()) {
                            Dialog d = new Dialog("Inscription Failed");
                            TextArea popupBody = new TextArea("you are already subscribe in this Competition", 3, 15);
                            popupBody.setUIID("PopupBody");
                            popupBody.setEditable(false);
                            d.setLayout(new BorderLayout());
                            d.add(BorderLayout.CENTER, popupBody);
                            final Button showPopup = new Button("Show Popup");
                            d.showPopupDialog(showPopup);

                        } else {
                            updateNbrJoueur(M.getNBR_MAX_JOUEURS() - 1, M.getIdCompetition());
                            updateIdCompetition(id, id1);
                            Dialog d = new Dialog("Confirmation");
                            TextArea popupBody = new TextArea("Inscription Succesful!!good luck", 3, 15);
                            popupBody.setUIID("PopupBody");
                            popupBody.setEditable(false);
                            d.setLayout(new BorderLayout());
                            d.add(BorderLayout.CENTER, popupBody);
                            final Button showPopup = new Button("Show Popup");
                            d.showPopupDialog(showPopup);
                        }
                    }
                }
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/CompetitionInscription.php?id=" + id + "&id1=" + Authentification.getM().getId());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
