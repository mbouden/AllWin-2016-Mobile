/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.CarteDeRecharge;
import Entity.Fan;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.mycompany.myapp.CarteRechargeForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author WiKi
 */
public class CartederechargeDAO {

    public void updateCarte(CarteDeRecharge C) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {

            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/updatecarte.php?id=" + C.getId() + "&etat=" + C.getEtat());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void recharger() {
        Form fc = new CarteRechargeForm();
        TextField cd = new TextField("", "Code de Recharge", 20, TextField.NUMERIC);
        Button bt = new Button("Envoyer");
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String code = cd.getText();
                ConnectionRequest connectionRequest;
                connectionRequest = new ConnectionRequest() {
                    List<CarteDeRecharge> Cartes = new ArrayList<>();
                    float x = 0;
                    Fan F = new Fan();

                    @Override
                    protected void readResponse(InputStream in) throws IOException {

                        JSONParser json = new JSONParser();

                        try {
                            Reader reader = new InputStreamReader(in, "UTF-8");

                            Map<String, Object> data = json.parseJSON(reader);

                            List<Map<String, Object>> content
                                    = (List<Map<String, Object>>) data.get("root");
                            Cartes.clear();
                            for (Map<String, Object> obj : content) {
                                CarteDeRecharge C = new CarteDeRecharge();

                                C.setId(Integer.parseInt((String) obj.get("idCarte")));
                                C.setEtat((String) obj.get("etat"));
                                Cartes.add(C);
                                System.out.println(C);
                            }

                        } catch (IOException err) {
                            Log.e(err);
                        }
                    }

                    @Override
                    protected void postResponse() {

                        for (CarteDeRecharge J : Cartes) {

                            if (J.getEtat().equals("recharger")) {
                                Dialog d = new Dialog("error");
                                TextArea popupBody = new TextArea("reload not successuf ", 3, 15);
                                popupBody.setUIID("PopupBody");
                                popupBody.setEditable(false);
                                d.setLayout(new BorderLayout());
                                d.add(BorderLayout.CENTER, popupBody);
                                final Button showPopup = new Button("Show Popup");
                                d.showPopupDialog(showPopup);

                            } else {
                                F =(Fan) Authentification.getM();
                                F.setCredit(F.getCredit()+10);
                                new TicketDAO().updateCredit((Fan) Authentification.getM());
                                J.setEtat("recharger");
                                updateCarte(J);
                                Dialog d = new Dialog("successeful");
                                TextArea popupBody = new TextArea("reload successeful ", 3, 15);
                                popupBody.setUIID("PopupBody");
                                popupBody.setEditable(false);
                                d.setLayout(new BorderLayout());
                                d.add(BorderLayout.CENTER, popupBody);
                                final Button showPopup = new Button("Show Popup");
                                d.showPopupDialog(showPopup);
                            }
                        }
                    }
                };
                connectionRequest.setUrl("https://allwin.000webhostapp.com/rechargerCompte.php?code=" + code + "&id=" + Authentification.getM().getId());
                NetworkManager.getInstance().addToQueue(connectionRequest);
            }
        });
        fc.add(cd);
        fc.add(bt);
        fc.show();
    }
}
