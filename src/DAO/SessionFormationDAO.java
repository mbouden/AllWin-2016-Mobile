/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Compte;
import com.codename1.io.ConnectionRequest;
import java.util.ArrayList;
import java.util.List;
import Entity.SessionDeFormation;
import com.codename1.components.MultiButton;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.SessionFormationForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author bilel
 */
public class SessionFormationDAO {

    int ch;
    StringBuffer sb = null;
    Button registerButton;
    Button bt = new Button("Go Back");
    Button bt2 = new Button("Go Back");
    SessionFormationForm f;
    Container cnt2;
    Form liste;
    static List<Arbitre> listeInscrits;

    public void getSession() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<SessionDeFormation> lsf = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");
                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> listeSession = (List<Map<String, Object>>) data.get("root");
                    lsf.clear();
                    for (Map<String, Object> obj : listeSession) {
                        String dateTime = (String) obj.get("dateDebutFormation");
                        String dateTime2 = (String) obj.get("dateFinFormation");
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = dateParser.parse(dateTime);
                        Date date2 = dateParser.parse(dateTime2);
                        SessionDeFormation session = new SessionDeFormation(date, date2, Integer.parseInt((String) obj.get("NbrCandidat")), Integer.parseInt((String) obj.get("prixFormation")), (String) obj.get("lieuFormation"));
                        session.setIdFormation(Integer.parseInt((String) obj.get("idSessionFormation")));
                        lsf.add(session);
                    }
                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }

            @Override
            protected void postResponse() {
                f = new SessionFormationForm();
                f.removeAll();
                for (SessionDeFormation session : lsf) {

                    MultiButton mb = new MultiButton();
                    mb.setTextLine1("Lieu Formation " + session.getLieu());
                    mb.setTextLine2("Prix Formation " + session.getPrix());
                    mb.setTextLine3("Date Debut " + session.getDateDebut());
                    mb.setTextLine4("Date Fin " + session.getDateFin());
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(150);
                    f.add(mb);
                    mb.addActionListener(e -> {
//                        UIBuilder ui = new UIBuilder();
//                        cnt2 = (ui.createContainer(MyApplication.getTheme(), "GUIRegister"));
//                        list2 = (Form) cnt2;
//                        list2.setTitle("Liste De Session");
                        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                        c.setScrollableY(true);
                        Container co = new Container();
                        co.add(createCont(session));
                        c.add(co);
                        Command myWorld = new Command("");
                        liste = new Form("Details Sessions");
                        //list2 = new Form();
                        liste.add(c);
                        liste.getToolbar().setBackCommand(myWorld);
                        liste.addCommandListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent evt) {
                                if (evt.getCommand() == myWorld) {
                                    getSession();
                                }
                            }
                        });
                        
                        
                        liste.show();
                        //  System.out.println(session);
//                        System.out.println("****"+getListeInscrits().get(0));
                        if (Dialog.show("Inscription", "Voulez vous s'inscrire à cette session  ", "Yes", "NO")) {

                            registerSession(session.getIdFormation(), Authentification.getM().getId());
                        }
                    });
                }
                f.show();
            }
        };
        connectionRequest.setUrl("http://allwin.000webhostapp.com/getSession.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);

    }

    public void registerSession(Integer idSession, Integer idArbitre) {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream input) throws IOException {
                while ((ch = input.read()) != -1) {
                    //sb.append((char) ch);
                }
            }

            @Override
            protected void postResponse() {
//                System.out.println(sb.toString());
            }
        };
        connectionRequest.setUrl("http://allwin.000webhostapp.com/registerSession.php?idSession=" + idSession + "&idArbitre=" + Authentification.getM().getId());
        NetworkManager.getInstance().addToQueue(connectionRequest);
        //Dialog.show(":D", sb.toString(), "OK", null);

    }

    public Container createCont(SessionDeFormation s) {
        Label lieu = new Label("Session à: " + s.getLieu());
//         img = new Label(Image.createImage("/location.png"));
//        } catch (IOException ex) {
//        }//  Label img = null;
//        try {
//            img = new Label(Image.createImage("/location.png"));
//        } catch (IOException ex) {
//        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        Label date = new Label("Debut: " + dateFormatter.format(s.getDateDebut()));
        Label date2 = new Label("Fin: " + dateFormatter.format(s.getDateFin()));
        Label prix = new Label("Nombre des Candidats Max: " + String.valueOf(s.getNbrCandidats()));
        Label nbr = new Label("Prix de la Session: " + String.valueOf(s.getPrix()));
        Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container ctx = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctliste = new Container();

        ctx.add(date);
        ctx.add(date2);
        ct.add(lieu);
        ct.add(ctx);
        ct.add(prix);
        ct.add(nbr);
        ctliste.add(new Label("Liste Des Participants:"));
        ct.add(ctliste);
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Arbitre> listeInscrits = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");
                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> liste = (List<Map<String, Object>>) data.get("root");
                    listeInscrits.clear();
                    for (Map<String, Object> obj : liste) {
                        String birthday = (String) obj.get("date_naissance_membre");
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = dateParser.parse(birthday);
                        Arbitre A = new Arbitre(200, (String) obj.get("degre"), (String) obj.get("nom_membre"), (String) obj.get("prenom_membre"), date, "", new Compte((String) obj.get("email"), "", ""), Integer.parseInt((String) obj.get("num_tel_membre")));
                        listeInscrits.add(A);
                    }

                    System.out.println(listeInscrits);
                } catch (IOException err) {
                    Log.e(err);
                } catch (ParseException ex) {
                }
            }

            @Override
            protected void postResponse() {
                for (Arbitre arbitre : listeInscrits) {
                    Label arb = new Label("Arbitre: " + arbitre.getNom() + "  " + arbitre.getPrenom());
                    Label arb4 = new Label("Email: " + arbitre.getCompte().getMail());
                    Label arb2 = new Label("Degree: " + arbitre.getDegre());
                    Label arb3 = new Label("numero Tel: " + arbitre.getNumTel());
                    ct.setScrollableY(true);
                    ct.add(arb);
                    ct.add(arb2);
                    ct.add(arb3);
                    ct.add(arb4);
                }

            }

        };
        setListeInscrits(listeInscrits);
        System.out.println(listeInscrits);
        connectionRequest.setUrl("http://allwin.000webhostapp.com/fetchInscrits.php?idSession=" + s.getIdFormation());
        NetworkManager.getInstance().addToQueue(connectionRequest);

        Container ct3 = BorderLayout.center(ct);
        // ct3.add(BorderLayout.EAST, img);

        return ct3;
    }

    public static List<Arbitre> getListeInscrits() {
        return listeInscrits;
    }

    public static void setListeInscrits(List<Arbitre> listeInscrits) {
        SessionFormationDAO.listeInscrits = listeInscrits;
    }

}
