/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Club;
import Entity.Joueur;
import Entity.Membre;
import com.codename1.components.MultiButton;
import com.codename1.googlemaps.MapContainer;
//import com.codename1.googlemaps.MapContainer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.maps.Coord;
import com.codename1.maps.MapListener;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.ClubGUI;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Bilel Oueslati
 */
public class ClubDao {

    Command cmdBack, cmdBack2;
    Button bt, bt2;

    String nomclub;
    Form listJoueurs;
    Form detail = new Form("Blur Dialog", new BoxLayout(BoxLayout.Y_AXIS));

    public void getClub() {

        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Club> clubs = new ArrayList<>();
            List<Membre> membres = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    membres.clear();
                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    clubs.clear();

                    for (Map<String, Object> obj : content) {
                        clubs.add(
                                new Club(Integer.parseInt((String) obj.get("idClub")), ((String) obj.get("nomClub")), (String) obj.get("adresseClub"), (String) obj.get("mailClub"), Float.parseFloat((String) obj.get("laltClub")), Float.parseFloat((String) obj.get("longgClub")))
                        );
//                        int x=1;
//                        for (int i = 0; i < 4; i++) {
//                            membres.add(
//                                    new Membre((String) obj.get("nomMembre" + i), (String) obj.get("prenomMembre" + i), Integer.parseInt((String) obj.get("idClub"))));
//                            x++;
//                        }

                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                ClubGUI listClub = new ClubGUI();
                listClub.removeAll();
                listClub.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

                for (Club club : clubs) {
                    MultiButton mb = new MultiButton(club.getNom());
                    mb.setUIID("CheckBox");
                    mb.getStyle().setOpacity(150);
                    mb.setTextLine2(club.getAdresse());
                    listClub.setTitle("Clubs");

                    listClub.add(mb);
                    mb.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            nomclub = club.getNom();

//                         
                            detail = new Form();
                            Dialog.setDefaultBlurBackgroundRadius(8);
                            detail.setTitle(nomclub);
                            detail.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
                            Resources r = null;
                            try {
                                r = Resources.open("/theme.res");
                            } catch (IOException ex) {
                            }

                            Image imBa = r.getImage("back-command.png");
                            cmdBack = new Command("", imBa);
                            Label l = new Label("Club: " + club.getNom());
                            Label l2 = new Label("Adress: " + club.getAdresse());
                            Label l3 = new Label("Mail: " + club.getMail());
                            detail.getToolbar().addCommandToLeftBar(cmdBack);
                            detail.addCommandListener(e -> {
                                if (e.getCommand() == cmdBack) {
                                    listClub.showBack();
                                }
                            });
                            Container ct = new Container(new FlowLayout());
                            Image joueur = null;
                            try {
                                joueur = Image.createImage("/joueurs.png");
                            } catch (IOException ex) {
                            }
                            Button joueurs = new Button("Joueurs", joueur);
                            joueurs.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent evt) {
                                    int idclub;
                                    idclub = club.getId();
                                    showPlayers(idclub);
                                }
                            }
                            );
                            Image ins = null;
                            try {
                                ins = Image.createImage("/inscriptionn.png");
                            } catch (IOException ex) {
                            }
                            Button Inscription = new Button("S'inscrire", ins);
                            
                            Inscription.addActionListener(
                                    new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent evt
                                ) {
                                    Dialog.show("Validation !? ", "Vous Aimez Devenir un Joueur de " + club.getNom(), "Ok", "Cancel");
                                    System.out.println(club.getId());

                                    ConnectionRequest connectionRequest;
                                    connectionRequest = new ConnectionRequest() {
                                        @Override
                                        protected void readResponse(InputStream in) throws IOException {
                                        }

                                        @Override
                                        protected void postResponse() {
                                            
                                        }

                                    };

                                    connectionRequest.setUrl("https://allwin.000webhostapp.com/inscriptionclub.php?idclub=" + club.getId() + "&idMembre=" + Authentification.getM().getId());
                                    NetworkManager.getInstance().addToQueue(connectionRequest);
                                }
                            }
                            );
                            Image ma = null;
                            try {
                                ma = Image.createImage("/map.png");
                            } catch (IOException ex) {
                            }
                            Button localisation = new Button("Location", ma);

                            localisation.addActionListener(
                                    new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent evt) {
                                    Form local = new Form();
                                    local.setLayout(new BorderLayout());
                                    local.setTitle("Location");
                                    // Map

                                    final MapContainer cnt = new MapContainer();
                                    Coord center = new Coord(club.getLaltClub(), club.getLonggClub());
                                    cnt.zoom(center, 15);

                                    try {
                                        cnt.setCameraPosition(new Coord(club.getLaltClub(), club.getLonggClub()));
                                        cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(club.getLaltClub(), club.getLonggClub()), "Hi marker", "Optional long description", new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent evt) {
                                                Dialog.show("Marker Clicked!", "You clicked the marker", "OK", null);
                                            }
                                        });
                                    } catch (IOException err) {
                                        // since the image is iin the jar this is unlikely
                                        err.printStackTrace();
                                    }

                                    // local.addComponent(BorderLayout.CENTER, cnt);
                                    final Label lbl = new Label("Location: ... " + club.getAdresse());
                                    cnt.addMapListener(new MapListener() {
                                        public void mapPositionUpdated(Component source, int zoom, Coord center) {
                                            lbl.setText("Location: " + center.getLatitude() + ", " + center.getLongitude());
                                            lbl.setText("lalt: " + cnt.getCoordAtPosition(0, 0).getLatitude() + " / lon " + cnt.getCoordAtPosition(Display.getInstance().getDisplayWidth(), 0).getLongitude());
                                        }
                                    });

                                    cnt.addTapListener(new ActionListener() {

                                        @Override
                                        public void actionPerformed(ActionEvent evt) {

                                        }

                                    });

                                    cnt.addLongPressListener(new ActionListener() {

                                        public void actionPerformed(ActionEvent evt) {
                                            Dialog.show("Long Tap", "Long tap detected", "OK", null);
                                        }
                                    });
                                    local.addComponent(BorderLayout.SOUTH, lbl);
                                    local.addComponent(BorderLayout.CENTER, cnt);
//
//                                    local.addCommand(new Command("Move Camera") {
//                                        @Override
//                                        public void actionPerformed(ActionEvent ev) {
//                                            cnt.setCameraPosition(new Coord(club.getLaltClub(), club.getLonggClub()));
//                                            Coord center = new Coord(club.getLaltClub(), club.getLonggClub());
//                                            cnt.zoom(center, 15);
//                                        }
//                                    });

//                                    local.addCommand(new Command("Add Marker") {
//                                        public void actionPerformed(ActionEvent ev) {
//                                            try {
//                                                cnt.setCameraPosition(new Coord(club.getLaltClub(), club.getLonggClub()));
//                                                cnt.addMarker(EncodedImage.create("/maps-pin.png"), new Coord(club.getLaltClub(), club.getLonggClub()), "Hi marker", "Optional long description", new ActionListener() {
//                                                    public void actionPerformed(ActionEvent evt) {
//                                                        Dialog.show("Marker Clicked!", "You clicked the marker", "OK", null);
//                                                    }
//                                                });
//                                            } catch (IOException err) {
//                                                // since the image is iin the jar this is unlikely
//                                                err.printStackTrace();
//                                            }
//                                        }
//                                    });
//                                    local.addCommand(new Command("Clear All") {
//                                        public void actionPerformed(ActionEvent ev) {
//                                            cnt.clearMapLayers();
//                                        }
//                                    });
                                    Resources r = null;
                                    try {
                                        r = Resources.open("/theme.res");
                                    } catch (IOException ex) {
                                    }
                                    Image imBa2 = r.getImage("back-command.png");
                                    cmdBack2 = new Command("Back", imBa2);
                                    local.getToolbar().addCommandToLeftBar(cmdBack2);
                                    local.addCommandListener(e -> {
                                        if (e.getCommand() == cmdBack2) {
                                            detail.showBack();
                                        }
                                    });

                                    local.show();

                                }
                            });
                            
                            
                            Container ct2 = new Container (new BoxLayout(BoxLayout.Y_AXIS));
                            
                            ct.add(l);
                            ct.add(l2);
                            ct.add(l3);
                            ct2.add(ct);
                            ct2.add(joueurs);
                            ct2.add(localisation);
                            if (Authentification.getM() instanceof Joueur)
                                ct2.add(Inscription);
                            detail.add(ct2);
                            ct2.setUIID("Button");
                            ct2.getStyle().setOpacity(200);
                            detail.show();

                            
                        }
                    }
                    );

                }

                listClub.show();

            }
        };
        connectionRequest.setUrl(
                "https://allwin.000webhostapp.com/club.php");
        NetworkManager.getInstance()
                .addToQueue(connectionRequest);
    }

    public void showPlayers(int id) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Joueur> Joueurs = new ArrayList<>();

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    Joueurs.clear();
                    for (Map<String, Object> obj : content) {
                        Joueur c = new Joueur();
                        c.setId(Integer.parseInt((String) obj.get("idMembre")));
                        c.setNom((String) obj.get("nomMembre"));
                        c.setPrenom((String) obj.get("prenomMembre"));
                        c.setClassement(Integer.parseInt((String) obj.get("classement")));
                        Joueurs.add(c);
                    }

                } catch (IOException err) {
                    Log.e(err);

                }

            }

            @Override
            protected void postResponse() {
                listJoueurs = new Form(new BoxLayout(BoxLayout.Y_AXIS));
                listJoueurs.setTitle("Players");
                listJoueurs.removeAll();
                for (Joueur J : Joueurs) {
                    MultiButton mb = new MultiButton(J.getNom() + " " + J.getPrenom() +"       Classement: " + J.getClassement() );
                    mb.setUIID("CheckBox");
                    listJoueurs.add(mb);
                }
                Resources r = null;

                try {
                    r = Resources.open("/theme.res");
                } catch (IOException ex) {
                }
                Image imBa2 = r.getImage("back-command.png");
                cmdBack2 = new Command("Back", imBa2);

                listJoueurs.getToolbar()
                        .addCommandToLeftBar(cmdBack2);
                listJoueurs.addCommandListener(e
                        -> {
                    if (e.getCommand() == cmdBack2) {
                        detail.showBack();
                    }
                }
                );
                listJoueurs.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

                listJoueurs.show();
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/players.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
