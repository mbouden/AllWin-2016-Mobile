/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Joueur;
import Entity.Match;
import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Container;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.FeuilleDeMatchAddForm;
import com.mycompany.myapp.FeuilleMatchForm;
import com.mycompany.myapp.MyApplication;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mahmoud
 */
public class MatchNationalDAO {

    static Match m;

    public Match getMatchByID(int a) {
        m = new Match();
        ConnectionRequest cr;
        cr = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream input) throws IOException {
                List<Match> colors = new ArrayList<>();
                JSONParser json = new JSONParser();
                /*Reader reader = new InputStreamReader(input, "UTF-8");
                Map<String, Object> data = json.parseJSON(reader);
                Map<String, Object> content
                            = (Map<String, Object>) data.get("root");
                MatchNationalDAO.m = new Match((String) content.get("lieuMatch"), new Date(), new Date(),
                        new Date(), 50 , new Arbitre());
                MatchNationalDAO.m.setIdMatch(Integer.parseInt((String) content.get("idMatch")));*/
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    colors.clear();
                    for (Map<String, Object> obj : content) {

                        MatchNationalDAO.m = new Match((String) obj.get("lieuMatch"), new Date(), new Date(),
                                new Date(), 50, new Arbitre());
                        MatchNationalDAO.m.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));
                        colors.add(MatchNationalDAO.m);
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                System.out.println(MatchNationalDAO.m.getIdMatch());
            }
        };
        cr.setUrl("https://allwin.000webhostapp.com/matchById.php?idMatch=" + MatchNationalDAO.m.getIdMatch());
        NetworkManager.getInstance().addToQueue(cr);
        return MatchNationalDAO.m;
    }

    public void find() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Match> matches = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    matches.clear();

                    for (Map<String, Object> obj : content) {
                        List<Joueur> joueurs = new ArrayList<>();

                        String dateTime = (String) obj.get("dateMatch");
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = dateParser.parse(dateTime);

                        String dateTime1 = (String) obj.get("dateDebutTicket");

                        Date date1 = dateParser.parse(dateTime1);

                        String dateTime2 = (String) obj.get("dateFinTicket");
                        Date date2 = dateParser.parse(dateTime2);

                        String dateNaissanceJ1 = (String) obj.get("date_naissance_membre1");
                        SimpleDateFormat dateParser1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateNaiJ1 = dateParser1.parse(dateNaissanceJ1);

                        String dateNaissanceJ2 = (String) obj.get("date_naissance_membre2");
                        Date dateNaiJ2 = dateParser1.parse(dateNaissanceJ2);

                        String dateNaissanceA = (String) obj.get("date_naissance_membreA");
                        Date dateNaiA = dateParser1.parse(dateNaissanceA);

                        Joueur J1 = new Joueur((String) obj.get("nom_membre1"), (String) obj.get("prenom_membre1"), dateNaiJ1, (String) obj.get("sexe_membre1"), Integer.parseInt((String) obj.get("num_tel_membre1")));
                        Joueur J2 = new Joueur((String) obj.get("nom_membre2"), (String) obj.get("prenom_membre2"), dateNaiJ2, (String) obj.get("sexe_membre2"), Integer.parseInt((String) obj.get("num_tel_membre2")));
                        Arbitre A = new Arbitre((String) obj.get("nom_membreA"), (String) obj.get("prenom_membreA"), dateNaiA, (String) obj.get("sexe_membreA"), Integer.parseInt((String) obj.get("num_tel_membreA")));
                        joueurs.add(J1);
                        joueurs.add(J2);

                        Match m = new Match((String) obj.get("lieuMatch"), joueurs, date, date1, date2, Integer.parseInt((String) obj.get("nbrTickets")), A);

                        m.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));

                        matches.add(m);

                    }

                } catch (IOException | ParseException err) {

                }

            }

            @Override
            protected void postResponse() {

                FeuilleMatchForm f = new FeuilleMatchForm();

                for (Match match : matches) {
                    String s = match.getJoueurs().get(0).toString();
                    String d = match.getJoueurs().get(1).toString();
                    System.out.println(s);
                    System.out.println(d);
                    MultiButton mb = new MultiButton(s + " VS " + d);

                    mb.setTextLine2("Date: " + match.getDateMatch());
                    mb.setTextLine3("Nombre tickets :" + match.getNbrTicket());

                    f.add(mb);
                    mb.addActionListener(e -> {
                        FeuilleDeMatchAddForm c = new FeuilleDeMatchAddForm(match.getIdMatch());
                        c.show();
                    });

                }

                f.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/matchnationalematchsheet.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void findAll() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Match> matches = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    matches.clear();

                    for (Map<String, Object> obj : content) {
                        List<Joueur> joueurs = new ArrayList<>();

                        String dateTime = (String) obj.get("dateMatch");
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat dateParser1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date date = dateParser.parse(dateTime);

                        String dateTime1 = (String) obj.get("dateDebutTicket");

                        Date date1 = dateParser.parse(dateTime1);

                        String dateTime2 = (String) obj.get("dateFinTicket");
                        Date date2 = dateParser.parse(dateTime2);

                        String dateNaissanceJ1 = (String) obj.get("date_naissance_membre1");
                        
                        Date dateNaiJ1 = dateParser1.parse(dateNaissanceJ1);

                        String dateNaissanceJ2 = (String) obj.get("date_naissance_membre2");
                        Date dateNaiJ2 = dateParser1.parse(dateNaissanceJ2);

                        String dateNaissanceA = (String) obj.get("date_naissance_membreA");
                        Date dateNaiA = dateParser1.parse(dateNaissanceA);

                        Joueur J1 = new Joueur((String) obj.get("nom_membre1"), (String) obj.get("prenom_membre1"), dateNaiJ1, (String) obj.get("sexe_membre1"), Integer.parseInt((String) obj.get("num_tel_membre1")));
                        Joueur J2 = new Joueur((String) obj.get("nom_membre2"), (String) obj.get("prenom_membre2"), dateNaiJ2, (String) obj.get("sexe_membre2"), Integer.parseInt((String) obj.get("num_tel_membre2")));
                        Arbitre A = new Arbitre((String) obj.get("nom_membreA"), (String) obj.get("prenom_membreA"), dateNaiA, (String) obj.get("sexe_membreA"), Integer.parseInt((String) obj.get("num_tel_membreA")));
                        joueurs.add(J1);
                        joueurs.add(J2);

                        Match m = new Match((String) obj.get("lieuMatch"), joueurs, date, date1, date2, Integer.parseInt((String) obj.get("nbrTickets")), A);

                        m.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));

                        matches.add(m);

                    }

                } catch (IOException | ParseException err) {

                }

            }

            @Override
            protected void postResponse() {

                FeuilleMatchForm f = new FeuilleMatchForm();
                

                for (Match match : matches) {
                    Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                    
                    String s = match.getJoueurs().get(0).toString();
                    String d = match.getJoueurs().get(1).toString();
                    System.out.println(s);
                    System.out.println(d);
                    ct.add(new Label(s +" VS "+ d));
                    ct.add(new Label("Date: "+ match.getDateMatch()));
                    ct.setUIID("MyLabel");
                    f.add(ct);
                }

                f.show();
            }

        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/matchnational.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
}
