/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.FeuilleDeMatch;
import Entity.Joueur;
import Entity.Match;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.FeuilleDeMatchForm;
import com.mycompany.myapp.MyWorld;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author admin
 */
public class FeuilleDeMatchDAO {

    public void find() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<FeuilleDeMatch> feuilles = new ArrayList<>();

            @Override
            protected void readResponse(InputStream input) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    feuilles.clear();

                    for (Map<String, Object> obj : content) {
                        List<Joueur> joueurs = new ArrayList<>();

                        String dateTime = (String) obj.get("dateMatch");
                        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date date = dateParser.parse(dateTime);

                        String dateTime1 = (String) obj.get("dateDebutTicket");

                        Date date1 = dateParser.parse(dateTime1);

                        String dateTime2 = (String) obj.get("dateFinTicket");
                        Date date2 = dateParser.parse(dateTime2);

                        String dateNaissanceJ1 = (String) obj.get("date_naissance_membre1");
                        SimpleDateFormat dateParser1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateNaiJ1 = dateParser1.parse(dateNaissanceJ1);

                        String dateNaissanceJ2 = (String) obj.get("date_naissance_membre2");
                        Date dateNaiJ2 = dateParser1.parse(dateNaissanceJ2);

                        String dateNaissanceA = (String) obj.get("date_naissance_membreA");
                        Date dateNaiA = dateParser1.parse(dateNaissanceA);

                        Joueur J1 = new Joueur((String) obj.get("nom_membre1"), (String) obj.get("prenom_membre1"), dateNaiJ1, (String) obj.get("sexe_membre1"), Integer.parseInt((String) obj.get("num_tel_membre1")));
                        Joueur J2 = new Joueur((String) obj.get("nom_membre2"), (String) obj.get("prenom_membre2"), dateNaiJ2, (String) obj.get("sexe_membre2"), Integer.parseInt((String) obj.get("num_tel_membre2")));
                        Arbitre A = new Arbitre((String) obj.get("nom_membreA"), (String) obj.get("prenom_membreA"), dateNaiA, (String) obj.get("sexe_membreA"), Integer.parseInt((String) obj.get("num_tel_membreA")));
                        joueurs.add(J1);
                        joueurs.add(J2);

                        Match m = new Match((String) obj.get("lieuMatch"), joueurs, date, date1, date2, Integer.parseInt((String) obj.get("nbrTickets")), A);

                        m.setIdMatch(Integer.parseInt((String) obj.get("idMatch")));
                        FeuilleDeMatch f = new FeuilleDeMatch(m, (String) obj.get("score"), (String) obj.get("resultat"), (String) obj.get("etat"));
                        f.setIdFeuilleMatch(Integer.parseInt((String) obj.get("idFeuille")));

                        System.out.println(f.getMatch().getJoueurs());
                        feuilles.add(f);

                    }

                } catch (IOException | ParseException err) {

                }

            }

            @Override
            protected void postResponse() {
                Form f = new Form(new BoxLayout(BoxLayout.Y_AXIS));
                f.getToolbar().setBackCommand(new Command(""){
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        new MyWorld();
                    }
                });
                
                for (FeuilleDeMatch feuille : feuilles) {
                    Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                    String s = feuille.getMatch().getJoueurs().get(0).toString();
                    String d = feuille.getMatch().getJoueurs().get(1).toString();
                    
                    System.out.println(s);
                    System.out.println(d);
                    ct.add(new Label(s +" VS "+ d));
                    ct.add(new Label("Result: " + feuille.getScore() + " Score: " + feuille.getResultat()));
                    ct.add(new Label(feuille.getEtat()));
                    ct.setUIID("MyLabel");
                    f.add(ct);
                }
                f.show();
            }
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/feuilledematch.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public void add(String Score, String Result, int id) {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void readResponse(InputStream in) throws IOException {
                System.out.println(in);

            }

            @Override
            protected void postResponse() {
                FeuilleDeMatchForm f = new FeuilleDeMatchForm();
                
                f.show();
            }

        };

        connectionRequest.setUrl("https://allwin.000webhostapp.com/feuilledematchadd.php?Score=" + Score + "&Result=" + Result + "&Id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
}
