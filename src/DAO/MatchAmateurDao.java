/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Joueur;
import Entity.Match;
import Entity.Membre;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.MatchAmateurForm;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Bilel
 */
public class MatchAmateurDao {

    public void getMatchAmateur() {
        ConnectionRequest connectionRequest;
        connectionRequest = new ConnectionRequest() {
            List<Match> amateurs = new ArrayList<>();
            List<Membre> listMembre = new ArrayList<>();
            String a;
            Match m;

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(input, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);

                    List<Map<String, Object>> content
                            = (List<Map<String, Object>>) data.get("root");
                    amateurs.clear();
                    for (Map<String, Object> obj : content) {
                        try {
                            Joueur j1 = new Joueur();
                            Joueur j2 = new Joueur();
                            Arbitre A = new Arbitre();

                            A.setNom((String) obj.get("nomArbitre"));
                            A.setPrenom((String) obj.get("prenomArbitre"));

                            j1.setNom((String) obj.get("nomJoueur1"));
                            j1.setPrenom((String) obj.get("prenomJoueur1"));

                            j2.setNom((String) obj.get("nomJoueur2"));
                            j2.setPrenom((String) obj.get("prenomJoueur2"));
                            j1.setNom((String) obj.get("nomJoueur1"));
                            j1.setPrenom((String) obj.get("prenomJoueur1"));

                            j2.setNom((String) obj.get("nomJoueur2"));
                            j2.setPrenom((String) obj.get("prenomJoueur2"));
                            List<Joueur> listJoueurs = new ArrayList<>();
                            listJoueurs.add(j1);
                            listJoueurs.add(j2);

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            String d = (String) obj.get("dateMatch");
                            sdf.format(d);
                            Match m5 = new Match((String) obj.get("lieuMatch"),
                                    sdf.parse(d),
                                    new Date(), new Date(),0, A);
                            m5.setJoueurs(listJoueurs);
                            amateurs.add( m5);

                            System.out.println(amateurs);
                        } catch (ParseException ex) {
                        }
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }

            @Override
            protected void postResponse() {
                MatchAmateurForm f = new MatchAmateurForm();
                int i = 0;
                for (Match match : amateurs) {
                      Image mor = null;
                            try {
                                mor = Image.createImage("/more.png");
                            } catch (IOException ex) {
                            }
                    Button more = new Button("More...",mor);
                    String txt = "";
                    String txt2 = "";

                    txt2 = match.getDateMatch().toString();
                    more.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            Form detail = new Form();
                            detail.setTitle("Details");
                            detail.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
                            Resources r = null;
                            try {
                                r = Resources.open("/theme.res");
                            } catch (IOException ex) {
                            }

                            Image imBa = r.getImage("back-command.png");
                            Command cmdBack = new Command("Back", imBa);
                            
                           
                            
                            Label l;
                            l = new Label(match.getJoueurs().get(0) + " VS " + match.getJoueurs().get(1));
                            Label l2 = new Label("Refree: " + match.getArbitre().getNom() + " " + match.getArbitre().getPrenom());
                            Label l3 = new Label("Date: " + match.getDateMatch());
                            Label l4 = new Label("Place : " + match.getLieu());
                            detail.getToolbar().addCommandToLeftBar(cmdBack);
                            detail.addCommandListener(e -> {
                                if (e.getCommand() == cmdBack) {
                                   new MatchAmateurDao().getMatchAmateur();                                }
                            });
                            detail.setUIID("CheckBox");
                           
                            detail.add(l);
                            detail.add(l2);
                            detail.add(l3);
                            detail.add(l4);

                            detail.show();
                        }
                    });

                    txt = match.getJoueurs().get(0).toString() + " VS " + match.getJoueurs().get(1).toString();
                    int k = i + 1;

                    Label lb2 = new Label("     Date: " + txt2);
                    Label lb = new Label(k + " / " + match.getLieu() + "- " + txt);
                    lb.setUIID("CheckBox");

                    Container ct = new Container(new BoxLayout(BoxLayout.X_AXIS));

                    Container ct2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                    ct2.add(lb);
                    ct2.add(lb2);
                    ct2.add(more);

                    ct2.setUIID("CheckBox");

                    f.add(ct2);
                    i++;
                }

                f.show();

            }

        ;
        };
        connectionRequest.setUrl("https://allwin.000webhostapp.com/amateur.php");
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

}
