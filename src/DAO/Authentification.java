/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Arbitre;
import Entity.Club;
import Entity.Compte;
import Entity.Fan;
import Entity.Joueur;
import Entity.Medecin;
import Entity.Membre;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.mycompany.myapp.AuthentificationForm;
import com.mycompany.myapp.MyWorld;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mahmoud
 */
public class Authentification {

    private static Membre m;

    public void connect() {
        Container center = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container top = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container bot = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container border = BorderLayout.center(center);

        AuthentificationForm af = new AuthentificationForm();
        TextField username = new TextField("");
        username.setUIID("UserField");
        TextField password = new TextField("", "", 20, TextField.PASSWORD);
        password.setUIID("PassField");
        Button confirm = new Button("Connect");
        confirm.setUIID("LoginButton");
        Button signup = new Button("Sign Up ?");
        signup.setUIID("LoginButton");
        signup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new InscriptionDao().getAdd();
            }
        });
        
        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ConnectionRequest connectionRequest;
                connectionRequest = new ConnectionRequest() {
                    @Override
                    protected void readResponse(InputStream input) throws IOException {
                        JSONParser json = new JSONParser();
                        try {
                            Reader reader = new InputStreamReader(input, "UTF-8");
                            Map<String, Object> data = json.parseJSON(reader);
                            List<Map<String, Object>> content
                                    = (List<Map<String, Object>>) data.get("root");
                            for (Map<String, Object> obj : content) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String d = (String) obj.get("dateNaissance");
                                sdf.format(d);
                                Compte compte = new Compte((String) obj.get("mail"), password.getText(), username.getText());
                                if (obj.get("type").equals("Fan")) {
                                    try {
                                        m = new Fan(Float.parseFloat((String) obj.get("credit")), (String) obj.get("nomMembre"), (String) obj.get("prenomMembre"), sdf.parse(d), (String) obj.get("sexe"), compte, Integer.parseInt((String) obj.get("numTel")));
                                        m.setId(Integer.parseInt((String) obj.get("idMembre")));
                                    } catch (ParseException ex) {
                                    }
                                } else if (obj.get("type").equals("Joueur")) {
                                    try {
                                        Club club = new Club((String) obj.get("nomClub"), (String) obj.get("adresseClub"), (String) obj.get("mailClub"));
                                        club.setId(Integer.parseInt((String) obj.get("idClub")));
                                        m = new Joueur((String) obj.get("nomMembre"), (String) obj.get("prenomMembre"),
                                                sdf.parse(d), (String) obj.get("sexe"), compte, Integer.parseInt((String) obj.get("numTel")), 
                                                Integer.parseInt((String) obj.get("cin")),
                                                Integer.parseInt((String) obj.get("classement")),
                                                club, Integer.parseInt((String) obj.get("score")), (String) obj.get("pays"));
                                        m.setId(Integer.parseInt((String) obj.get("idMembre")));
                                    } catch (ParseException ex) {
                                    }
                                } else if (obj.get("type").equals("Arbitre")) {
                                    try {
                                        m = new Arbitre(Integer.parseInt((String) obj.get("cinMembre")), "", (String) obj.get("nomMembre"), (String) obj.get("prenomMembre"), sdf.parse(d), (String) obj.get("sexe"), compte, Integer.parseInt((String) obj.get("numTel")));
                                        m.setId(Integer.parseInt((String) obj.get("idMembre")));
                                    } catch (ParseException ex) {
                                    }
                                } else if (obj.get("type").equals("Medecin")) {
                                    try {
                                        m = new Medecin(Integer.parseInt((String) obj.get("cinMembre")), "", (String) obj.get("nomMembre"), (String) obj.get("prenomMembre"), sdf.parse(d), (String) obj.get("sexe"), compte, Integer.parseInt((String) obj.get("numTel")));
                                        m.setId(Integer.parseInt((String) obj.get("idMembre")));
                                    } catch (ParseException ex) {
                                    }
                                }
                                new MyWorld();
                            }
                        } catch (ClassCastException ex) {
                            Dialog.show("Error", "Wrong Username Or Password", "OK", null);
                        } catch (IOException err) {
                            Log.e(err);
                        }
                    }

                    @Override
                    protected void postResponse() {
                        
                    }
                };
                connectionRequest.setUrl("https://allwin.000webhostapp.com/authentification.php?username=" + username.getText() + "&password=" + password.getText());
                NetworkManager.getInstance().addToQueue(connectionRequest);
            }
        });
        
        af.add(username);
        af.add(password);
        af.add(confirm);
        af.add(signup);
        Image player = null;
        try {
            player = Image.createImage("/bg-login.png");
        } catch (IOException ex) {
        }
        af.getStyle().setBgImage(player, true);
        af.animateHierarchyFade(1000, 100);
        af.show();
    }

    public static Membre getM() {
        return m;
    }
}
