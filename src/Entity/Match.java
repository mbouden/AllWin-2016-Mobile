/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import com.codename1.l10n.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.util.Timestamp;
import java.util.Iterator;
import java.util.List;
//import java.util.Objects;

public class Match {

    private int idMatch;
    private String lieu;
    private List<Joueur> joueurs;
    private Date dateMatch;
    private Date dateDebutTicket;
    private Date dateFinTicket;
    private int nbrTicket;
    private Arbitre arbitre;

    public Match() {
        joueurs = new ArrayList<>();
    }

    public Match(String lieu, Date dateMatch, Date dateDebutTicket, Date dateFinTicket, int nbrTicket, Arbitre arbitre) {
        this();
        this.lieu = lieu;
        this.dateMatch = dateMatch;
        this.dateDebutTicket = dateDebutTicket;
        this.dateFinTicket = dateFinTicket;
        this.nbrTicket = nbrTicket;
        this.arbitre = arbitre;
    }
    
    public Match(String lieu, List<Joueur> joueurs, Date dateMatch, Date dateDebutTicket, Date dateFinTicket, int nbrTicket, Arbitre arbitre) {
        this.lieu = lieu;
        this.joueurs = joueurs;
        this.dateMatch = dateMatch;
        this.dateDebutTicket = dateDebutTicket;
        this.dateFinTicket = dateFinTicket;
        this.nbrTicket = nbrTicket;
        this.arbitre = arbitre;
    }

    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    public void setJoueurs(List<Joueur> joueurs) {
        this.joueurs = joueurs;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public String getLieu() {
        return lieu;
    }

    public Date getDateMatch() {
        return dateMatch;
    }

    public Date getDateDebutTicket() {
        return dateDebutTicket;
    }

    public Date getDateFinTicket() {
        return dateFinTicket;
    }

    public int getNbrTicket() {
        return nbrTicket;
    }

    public Arbitre getArbitre() {
        return arbitre;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setDateMatch(Date dateMatch) {
        this.dateMatch = dateMatch;
    }

    public void setDateDebutTicket(Date dateDebutTicket) {
        this.dateDebutTicket = dateDebutTicket;
    }

    public void setDateFinTicket(Date dateFinTicket) {
        this.dateFinTicket = dateFinTicket;
    }

    public void setNbrTicket(int nbrTicket) {
        this.nbrTicket = nbrTicket;
    }

    public void setArbitre(Arbitre arbitre) {
        this.arbitre = arbitre;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String resultat = "Match: "+this.lieu+" "+ sdf.format(dateMatch);
        return resultat;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Match other = (Match) obj;
//        if (!Objects.equals(this.dateMatch, other.dateMatch)) {
//            return false;
//        }
//        return Objects.equals(this.arbitre, other.arbitre);
//    }

}
