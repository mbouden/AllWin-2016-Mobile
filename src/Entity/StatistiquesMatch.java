/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;

public class StatistiquesMatch {

    private int id;
    private int idMatch;
    private int idMembre;
    private int nbrAces;
    private int nbrDoubleFaults;
    private float pPremierService;
    private int ptsPremierService; 
    private int ptsDeuxiemeService;
    private float balleBreak;
    private int ptsSurRetour;
    private int totalPtsGagnes;
    private Match match;
    private String nom;
    private String prenom;
    private Date date;
    
    public StatistiquesMatch() {
    }

    
    public StatistiquesMatch(int nbrAces, int nbrDoubleFaults, float pPremierService, int ptsDeuxiemeService, float balleBreak, int ptsSurRetour, int totalPtsGagnes, Match match) {
        this.nbrAces = nbrAces;
        this.nbrDoubleFaults = nbrDoubleFaults;
        this.pPremierService = pPremierService;
        this.ptsDeuxiemeService = ptsDeuxiemeService;
        this.balleBreak = balleBreak;
        this.ptsSurRetour = ptsSurRetour;
        this.totalPtsGagnes = totalPtsGagnes;
        this.match = match;
    }

    public StatistiquesMatch(int idMatch, int idMembre, int nbrAces, int nbrDoubleFaults, float pPremierService, int ptsPremierService, int ptsDeuxiemeService, float balleBreak, int ptsSurRetour, int totalPtsGagnes) {
        this.idMatch = idMatch;
        this.idMembre = idMembre;
        this.nbrAces = nbrAces;
        this.nbrDoubleFaults = nbrDoubleFaults;
        this.pPremierService = pPremierService;
        this.ptsPremierService = ptsPremierService;
        this.ptsDeuxiemeService = ptsDeuxiemeService;
        this.balleBreak = balleBreak;
        this.ptsSurRetour = ptsSurRetour;
        this.totalPtsGagnes = totalPtsGagnes;
    }

    public StatistiquesMatch(int nbrAces, int nbrDoubleFaults, float pPremierService, int ptsPremierService, int ptsDeuxiemeService, float balleBreak, int ptsSurRetour, int totalPtsGagnes,String nom,String prenom,Date date) {
        this.nbrAces = nbrAces;
        this.nbrDoubleFaults = nbrDoubleFaults;
        this.pPremierService = pPremierService;
        this.ptsPremierService = ptsPremierService;
        this.ptsDeuxiemeService = ptsDeuxiemeService;
        this.balleBreak = balleBreak;
        this.ptsSurRetour = ptsSurRetour;
        this.totalPtsGagnes = totalPtsGagnes;
        this.nom= nom;
        this.prenom = prenom;
        this.date = date;
    }

    public StatistiquesMatch(int idMatch) {
        this.idMatch = idMatch;
    }
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
    
    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public int getPtsPremierService() {
        return ptsPremierService;
    }

    public void setPtsPremierService(int ptsPremierService) {
        this.ptsPremierService = ptsPremierService;
    }
    
    
    
    
    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public int getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(int idMembre) {
        this.idMembre = idMembre;
    }

    

    public int getId() {
        return id;
    }

    public int getNbrAces() {
        return nbrAces;
    }

    public int getNbrDoubleFaults() {
        return nbrDoubleFaults;
    }

    public float getpPremierService() {
        return pPremierService;
    }

    public int getPtsDeuxiemeService() {
        return ptsDeuxiemeService;
    }

    public float getBalleBreak() {
        return balleBreak;
    }

    public int getPtsSurRetour() {
        return ptsSurRetour;
    }

    public int getTotalPtsGagnes() {
        return totalPtsGagnes;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNbrAces(int nbrAces) {
        this.nbrAces = nbrAces;
    }

    public void setNbrDoubleFaults(int nbrDoubleFaults) {
        this.nbrDoubleFaults = nbrDoubleFaults;
    }

    public void setpPremierService(float pPremierService) {
        this.pPremierService = pPremierService;
    }

    public void setPtsDeuxiemeService(int ptsDeuxiemeService) {
        this.ptsDeuxiemeService = ptsDeuxiemeService;
    }

    public void setBalleBreak(float balleBreak) {
        this.balleBreak = balleBreak;
    }

    public void setPtsSurRetour(int ptsSurRetour) {
        this.ptsSurRetour = ptsSurRetour;
    }

    public void setTotalPtsGagnes(int totalPtsGagnes) {
        this.totalPtsGagnes = totalPtsGagnes;
    }

    @Override
    public String toString() {
        return "StatistiquesMatch{" + "id=" + id + ", nbrAces=" + nbrAces + ", nbrDoubleFaults=" + nbrDoubleFaults + ", pPremierService=" + pPremierService + ", ptsDeuxiemeService=" + ptsDeuxiemeService + ", balleBreak=" + balleBreak + ", ptsSurRetour=" + ptsSurRetour + ", totalPtsGagnes=" + totalPtsGagnes + ", match=" + match + '}';
    }

   
}
