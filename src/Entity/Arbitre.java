/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Arbitre extends Membre {

    private int cin;
    private int numService;
    List<Match> MatchArbitre;
    private String degre;

    public Arbitre() {
        MatchArbitre = new ArrayList<>();
    }

    public Arbitre(int cin, String nom, String prenom, Date dateNaissance, String sexe, Compte compte, int numTel, String degre) {
        super(nom, prenom, dateNaissance, sexe, compte, numTel);
        this.cin = cin;
        this.degre = degre;
        MatchArbitre = new ArrayList<>();

    }
    
    public Arbitre(String nom, String prenom, Date dateNaissance, String sexe, int numTel) {
        super(nom, prenom, dateNaissance, sexe, numTel);
    }

    public List<Match> getMatchArbitre() {
        return MatchArbitre;
    }

    public void setMatchArbitre(List<Match> MatchArbitre) {
        this.MatchArbitre = MatchArbitre;
    }

    public String getDegre() {
        return degre;
    }

    public void setDegre(String degre) {
        this.degre = degre;
    }

    public Arbitre(String nom, String prenom) {
        super(nom, prenom);
    }

    public Arbitre(int cin, int numService, String degre) {
        this.cin = cin;
        this.numService = numService;
        this.degre = degre;
    }

    public Arbitre(int cin, String degre, String nom, String prenom, Date dateNaissance, String sexe, Compte compte, int numTel) {
        super(nom, prenom, dateNaissance, sexe, compte, numTel);
        this.cin = cin;
        this.degre = degre;
    }

    public int getCin() {
        return cin;
    }

    public int getNumService() {
        return numService;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public void setNumService(int numService) {
        this.numService = numService;
    }

    @Override
    public String toString() {
        String resultat = "Arbitre: "+this.nom+" "+this.prenom;
        return resultat;
    }
}
