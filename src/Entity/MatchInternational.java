/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;
import java.util.Date;

/**
 *
 * @author Bilel
 */
public class MatchInternational extends Match{
    private String evenementInternational;
    
    public MatchInternational(){
        
    }

    public MatchInternational(String evenementInternational, String lieu, Date dateMatch, Date dateDebutTicket, Date dateFinTicket, int nbrTicket, Arbitre arbitre) {
        super(lieu, dateMatch, dateDebutTicket, dateFinTicket, nbrTicket, arbitre);
        this.evenementInternational = evenementInternational;
    }

    public String getEvenementInternational() {
        return evenementInternational;
    }

    public void setEvenementInternational(String evenementInternational) {
        this.evenementInternational = evenementInternational;
    }

    @Override
    public String toString() {
        return super.toString()+"MatchInternational{" + "evenementInternational=" + evenementInternational + '}';
    }
    
   
}