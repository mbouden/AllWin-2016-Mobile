/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Bilel
 */
public class Joueur extends Membre {
    int id;
    private int cin;
    private int numLicense;
    private int classement;
    private Date dateLicense;
    List<Match> matchs;
    private Club club;
    private int score;
    private String pays;

    public Joueur() {
        super();
        matchs = new ArrayList<>();
    }

    public Joueur(String nom, String prenom, Date dateNaissance, String sexe, Compte compte, int numTel,int cin, int classement, Club club, int score, String pays) {
        super( nom,  prenom,  dateNaissance,  sexe,  compte,  numTel);
        this.cin = cin;
        this.classement = classement;
        this.club = club;
        this.score = score;
        this.pays = pays;
    }
    
    public Joueur(int cin, int classement, Club club, int score, String pays, String nom, String prenom, Date dateNaissance, String sexe, Compte compte, int numTel) {
        super(nom, prenom, dateNaissance, sexe, compte, numTel);
        this.cin = cin;
        this.numLicense = numLicense;
        this.classement = classement;
        this.dateLicense = dateLicense;
        this.club = club;
        this.score = score;
        this.pays = pays;
        matchs = new ArrayList<>();

    }
    
    public Joueur(int id) {
        this.id = id;
    }
    
    public Joueur(String nom, String prenom, Date dateNaissance, String sexe, int numTel) {
        super(nom, prenom, dateNaissance, sexe, numTel);
    }

    public int getCin() {
        return cin;
    }

    public int getNumLicense() {
        return numLicense;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public int getClassement() {
        return classement;
    }

    public Date getDateLicense() {
        return dateLicense;
    }

    public Club getClub() {
        return club;
    }

    public int getScore() {
        return score;
    }

    public String getPays() {
        return pays;
    }

    public void setCin(int cin) {
        this.cin = cin;
    }

    public void setNumLicense(int numLicense) {
        this.numLicense = numLicense;
    }

    public void setClassement(int classement) {
        this.classement = classement;
    }

    public void setDateLicense(Date dateLicense) {
        this.dateLicense = dateLicense;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public List<Match> getMatchs() {
        return matchs;
    }

    public void setMatchs(List<Match> matchs) {
        this.matchs = matchs;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.cin;
        return hash;
    }

    @Override
    public String toString() {
       return this.nom+" "+this.prenom;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Joueur other = (Joueur) obj;
        if (this.cin != other.cin) {
            return false;
        }
        return true;
    }

}
