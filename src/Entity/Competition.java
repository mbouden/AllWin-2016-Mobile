/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author admin
 */
public class Competition {

    List<Match> matchsCompetition ;
    List<Joueur> joueursCompetition ;
    private Date date_debut;
    private Date date_fin;
    private String lieu;
    private String nom;
    private int idCompetition;
    private int NBR_MAX_JOUEURS;

    public Competition() {
        matchsCompetition = new ArrayList<>();
        joueursCompetition = new ArrayList<>();
    }

    public Competition(String nom,Date date_debut, Date date_fin, String lieu, int NBR_MAX_JOUEURS) {
        this();
        this.nom=nom;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.lieu = lieu;
        this.NBR_MAX_JOUEURS = NBR_MAX_JOUEURS;
    }

    public List<Match> getMatchsCompetition() {
        return matchsCompetition;
    }

    public void setMatchsCompetition(List<Match> matchsCompetition) {
        this.matchsCompetition = matchsCompetition;
    }

    public List<Joueur> getJoueursCompetition() {
        return joueursCompetition;
    }

    public void setNBR_MAX_JOUEURS(int NBR_MAX_JOUEURS) {
        this.NBR_MAX_JOUEURS = NBR_MAX_JOUEURS;
    }

    public void setJoueursCompetition(List<Joueur> joueursCompetition) {
        this.joueursCompetition = joueursCompetition;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public String getLieu() {
        return lieu;
    }

    public int getIdCompetition() {
        return idCompetition;
    }

    public int getNBR_MAX_JOUEURS() {
        return NBR_MAX_JOUEURS;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setIdCompetition(int idCompetition) {
        this.idCompetition = idCompetition;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Competition other = (Competition) obj;
        return this.idCompetition == other.idCompetition;
    }

    @Override
    public String toString() {
        String resultat="Competition{ Nom=" +nom+ "date_debut=" + date_debut + ", date_fin=" + date_fin + ", lieu=" + lieu + ", idCompetition=" + idCompetition  + '}';
        Match e;
        Iterator <Match>it;
        it=matchsCompetition.iterator();
        while(it.hasNext())
        {
        e=it.next();
        resultat+="/l'id du match=  "+e.getIdMatch();
        }
        
        Joueur j;
        Iterator <Joueur> itt;
        itt=joueursCompetition.iterator();
        while(itt.hasNext())
        {
        j=itt.next();
        resultat+="/l'id Joueur="+j.getId();
        }
        
        return   resultat;
    
    }

    
}
