/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author WiKi
 */
public class CarteDeRecharge {
    int id ;
    int code ;
    float credit ;
    String etat ;

    public CarteDeRecharge() {
    }

    public CarteDeRecharge(int code, float credit, String etat) {
        this.code = code;
        this.credit = credit;
        this.etat = etat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }


    @Override
    public String toString() {
        return "CarteDeRecharge{" + "code=" + code + ", credit=" + credit + ", etat=" + etat + '}';
    }
    
    
    
}
