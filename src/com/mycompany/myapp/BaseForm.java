/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.FeuilleDeMatchDAO;
import DAO.MatchNationalDAO;
import com.codename1.components.ScaleImageLabel;
import com.codename1.messaging.Message;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.layouts.Layout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 *
 * @author admin
 */
public class BaseForm extends Form {

    public BaseForm() {
    }

    public BaseForm(Layout contentPaneLayout) {
        super(contentPaneLayout);
    }

    public BaseForm(String title, Layout contentPaneLayout) {
        super(title, contentPaneLayout);
    }

    public Component createLineSeparator() {
        Label separator = new Label("", "WhiteSeparator");
        separator.setShowEvenIfBlank(true);
        return separator;
    }

    public Component createLineSeparator(int color) {
        Label separator = new Label("", "WhiteSeparator");
        separator.getUnselectedStyle().setBgColor(color);
        separator.getUnselectedStyle().setBgTransparency(255);
        separator.setShowEvenIfBlank(true);
        return separator;
    }

    protected void addSideMenu(Resources res) {
        Toolbar tb = getToolbar();
        Image img = res.getImage("tenniss.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        tb.addComponentToSideMenu(LayeredLayout.encloseIn(
                sl,
                FlowLayout.encloseCenterBottom(
                        new Label(res.getImage("tenniss.png"), "PictureWhiteBackgrond"))
        ));
        tb.addMaterialCommandToSideMenu("Home", FontImage.MATERIAL_ARCHIVE, e -> new NewsfeedForm(res).showBack());
        tb.addMaterialCommandToSideMenu("National Matchs", FontImage.MATERIAL_VISIBILITY, e -> new MatchNationalDAO().findAll());
        tb.addMaterialCommandToSideMenu("Matchs Sheets", FontImage.MATERIAL_VISIBILITY, e -> new FeuilleDeMatchDAO().find());
        tb.addMaterialCommandToSideMenu("Add Matchs Sheets", FontImage.MATERIAL_ADD, e -> new MatchNationalDAO().find());
        tb.addMaterialCommandToSideMenu("Contact", FontImage.MATERIAL_CONTACTS, e -> new ContacterAddForm().show());
        tb.addMaterialCommandToSideMenu("Send-Email", FontImage.MATERIAL_EMAIL, e -> Display.getInstance().sendMessage(new String[]{"mohamedmouldi.slama@esprit.tn"}, "Contact", new Message("Hello World")));
        tb.addMaterialCommandToSideMenu("Send-SMS", FontImage.MATERIAL_SMS, e -> new SmsForm().show());
        tb.addMaterialCommandToSideMenu("Profile", FontImage.MATERIAL_SETTINGS, e -> new WalkthruForm(res).show());
        tb.addMaterialCommandToSideMenu("Logout", FontImage.MATERIAL_EXIT_TO_APP, e -> new WalkthruForm(res).show());
    }
}
