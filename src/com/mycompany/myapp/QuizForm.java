/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.LienStreamingDAO;
import DAO.PariDAO;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;

/**
 *
 * @author Mahmoud
 */
public class QuizForm extends Form{

    public QuizForm() {
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        this.setTitle("Quizz !");
        
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {}
        
        Command myWorld = new Command("My World",home);
        this.getToolbar().addCommandToSideMenu(myWorld);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()==myWorld) {
                    new MyWorld().show();
                }
            }
        });
        
        Image youtube = null;
        try {
            youtube = Image.createImage("/youtube.png");
        } catch (IOException ex) {}
        
        Command cmdLien = new Command("Streaming",youtube);
        this.getToolbar().addCommandToSideMenu(cmdLien);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()==cmdLien) {
                    new LienStreamingDAO().findAll();
                }
            }
        });
        
        Image bet = null;
        try {
            bet = Image.createImage("/tennis-ball.png");
        } catch (IOException ex) {}
        
        Command cmdBet = new Command("Betting",bet);
        this.getToolbar().addCommandToSideMenu(cmdBet);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()==cmdBet) {
                    new PariDAO().afficher();
                }
            }
        });
    }
    
}
