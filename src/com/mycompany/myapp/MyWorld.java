/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.Authentification;
import DAO.CartederechargeDAO;
import DAO.ClubDao;
import DAO.CompetitionDAO;
import DAO.CompteRenduDAO;
import DAO.FeuilleDeMatchDAO;
import DAO.LienStreamingDAO;
import DAO.MatchAmateurDao;
import DAO.MatchInternationalDAO;
import DAO.MatchNationalDAO;
import DAO.MembreDAO;
import DAO.PariDAO;
import DAO.ProfileDAO;
import DAO.Quiz;
import DAO.SessionFormationDAO;
import DAO.StatMatchDao;
import DAO.TicketDAO;
import Entity.Arbitre;
import Entity.Fan;
import Entity.Joueur;
import Entity.Medecin;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TTFFont;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import java.io.IOException;

/**
 *
 * @author Mahmoud
 */
public class MyWorld extends Form {

    public MyWorld() {

        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        Label welcome = new Label("Welcome " + Authentification.getM().getNom() + " " + Authentification.getM().getPrenom()+", To Your World Of Tennis !");
        
        //Label welcome2 = new Label("To Your World Of Tennis !");
        Container wc = new Container(new FlowLayout());
        wc.add(welcome);
        wc.getStyle().setOpacity(200);
        TTFFont font = null;
        try {
            font = TTFFont.createFont("MyFont", "/MyFont.ttf");
            welcome.getAllStyles().setFont(font);
            //welcome2.getAllStyles().setFont(font);
            wc.getAllStyles().setFont(font);
        } catch (IOException ex) {
        }

        Container ct = new Container(new FlowLayout());
        /*                  Fan Section                     */
        if (Authentification.getM() instanceof Fan) {
            Image icon = null;
            try {
                icon = Image.createImage("/chips.png");
            } catch (IOException ex) {
            }
            Button btnPari = new Button(icon);
            btnPari.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new PariDAO().afficher();
                }
            });
            btnPari.setUIID("MyButton");

            Image icon2 = null;
            try {
                icon2 = Image.createImage("/stick-man.png");
            } catch (IOException ex) {
            }
            Button btnQuiz = new Button(icon2);
            btnQuiz.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new Quiz().getQuiz();
                }
            });
            btnQuiz.setUIID("MyButton");

            Image stream = null;
            try {
                stream = Image.createImage("/youtube2.png");
            } catch (IOException ex) {
            }
            Button btnStream = new Button(stream);
            btnStream.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new LienStreamingDAO().findAll();
                }
            });
            btnStream.setUIID("MyButton");
            
            Image test = null;
            try {
                test = Image.createImage("/pie-chart.png");
            } catch (IOException ex) {
            }
            Button btnTest = new Button(test);
            btnTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new StatMatchDao().getPieMatch();
                }
            });
            btnTest.setUIID("MyButton");

            Image carte = null;
            try {
                carte = Image.createImage("/wallet.png");
            } catch (IOException ex) {
            }
            Button btnCarte = new Button(carte);
            btnCarte.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new CartederechargeDAO().recharger();
                }
            });
            btnCarte.setUIID("MyButton");

            Image ticket = null;
            try {
                ticket = Image.createImage("/ticket.png");
            } catch (IOException ex) {
            }
            Button btnTicket = new Button(ticket);
            btnTicket.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new TicketDAO().getMatchs();
                }
            });
            btnTicket.setUIID("MyButton");

            Image facebook = null;
            try {
                facebook = Image.createImage("/facebook.png");
            } catch (IOException ex) {
            }
            Button btnFB = new Button(facebook);
            btnFB.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new MembreDAO().ConnectFB();
                }
            });
            Fan fan = (Fan) Authentification.getM();
            btnFB.setUIID("MyButton");
            Image coin;
            try {
                coin = Image.createImage("/coin.png");
                Label credit = new Label(fan.getCredit()+"DT",coin);
                credit.getAllStyles().setFont(font);
                this.add(FlowLayout.encloseRightBottom(credit));
            } catch (IOException ex) {
            }

            ct.add(btnPari);
            ct.add(btnStream);
            ct.add(btnFB);
            ct.add(btnQuiz);
            ct.add(btnTest);
            ct.add(btnTicket);
            ct.add(btnCarte);
        }

        /*           Medic Section                     */
        if (Authentification.getM() instanceof Medecin) {
            
            Image sheet = null;
            try {
                sheet = Image.createImage("/copy.png");
            } catch (IOException ex) {
            }
            Button btnSheet = new Button(sheet);
            btnSheet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new CompteRenduDAO().getstatCompteRendu();
                }
            });
            
            btnSheet.setUIID("MyButton");
            ct.add(btnSheet);
        }

        /*           Referee Section                   */
        if (Authentification.getM() instanceof Arbitre) {
            Image feuille = null;
            try {
                feuille = Image.createImage("/scoreboard.png");
            } catch (IOException ex) {
            }
            Button btnFeuille = new Button(feuille);
            btnFeuille.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new MatchNationalDAO().findAll();
                }
            });
            btnFeuille.setUIID("MyButton");
            
            Image session = null;
            try {
                session = Image.createImage("/diploma.png");
            } catch (IOException ex) {
            }
            Button btnSession = new Button(session);
            btnSession.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new SessionFormationDAO().getSession();
                }
            });
            btnSession.setUIID("MyButton");

            Image sheet = null;
            try {
                sheet = Image.createImage("/copy.png");
            } catch (IOException ex) {
            }
            Button btnSheet = new Button(sheet);
            btnSheet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new FeuilleDeMatchDAO().find();
                }
            });
            
            btnSheet.setUIID("MyButton");
            
            Image sms = null;
            try {
                sms = Image.createImage("/contacts.png");
            } catch (IOException ex) {
            }
            Button btSms = new Button(sms);
            btSms.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    new SmsForm().show();
                }
            });
            btSms.setUIID("MyButton");

            ct.add(btnSession);
            ct.add(btnSheet);
            ct.add(btnFeuille);
            ct.add(btSms);
        }

        /*           Player Section                    */
        if (Authentification.getM() instanceof Joueur) {

        }

        /*           Common Sections                   */
        Image user = null;
        try {
            user = Image.createImage("/user.png");
        } catch (IOException ex) {
        }
        Button btnUser = new Button(user);
        btnUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new ProfileDAO().getProfile();
            }
        });
        btnUser.setUIID("MyButton");

        Image matchInter = null;
        try {
            matchInter = Image.createImage("/internet.png");
        } catch (IOException ex) {
        }
        Button btnMatchInter = new Button(matchInter);
        btnMatchInter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new MatchInternationalDAO().getMatchInternational();
            }
        });
        btnMatchInter.setUIID("MyButton");

        Image match = null;
        try {
            match = Image.createImage("/match.png");
        } catch (IOException ex) {
        }
        Button btnMatch = new Button(match);
        btnMatch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new MatchAmateurDao().getMatchAmateur();
            }
        });
        btnMatch.setUIID("MyButton");

        Image club = null;
        try {
            club = Image.createImage("/c.png");
        } catch (IOException ex) {
        }
        Button btnClub = new Button(club);
        btnClub.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new ClubDao().getClub();
            }
        });
        btnClub.setUIID("MyButton");

        Image tourn = null;
        try {
            tourn = Image.createImage("/cup.png");
        } catch (IOException ex) {
        }
        Button btnTourn = new Button(tourn);
        btnTourn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (!(Authentification.getM() instanceof Joueur)) {
                    new CompetitionDAO().getCompetitions();
                } else {
                    new CompetitionDAO().CompetitionsToInscription();
                }
            }
        });
        btnTourn.setUIID("MyButton");

        Image contact = null;
        try {
            contact = Image.createImage("/contacts.png");
        } catch (IOException ex) {
        }
        Button btnContact = new Button(contact);
        btnContact.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new ContacterAddForm().show();
            }
        });
        btnContact.setUIID("MyButton");
        
        Image mail = null;
        try {
            mail = Image.createImage("/letter.png");
        } catch (IOException ex) {
        }
        Command cmd;
        cmd = new Command("", mail){
            @Override
            public void actionPerformed(ActionEvent evt) {
                Display.getInstance().sendMessage(new String[]{"mohamedmouldi.slama@esprit.tn"}, "Contact", new Message("All Win !!"));
            }
        };
        this.getToolbar().addCommandToRightBar(cmd);
        
        ct.add(btnTourn);
        ct.add(btnClub);
        ct.add(btnMatch);
        ct.add(btnMatchInter);
        ct.add(btnContact);
        ct.add(btnUser);

        this.add(wc);
        this.add(ct);
        this.show();
    }
}
