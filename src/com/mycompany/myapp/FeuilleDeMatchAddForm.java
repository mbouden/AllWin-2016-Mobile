/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.FeuilleDeMatchDAO;
import DAO.MatchNationalDAO;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import java.io.IOException;

/**
 *
 * @author admin
 */
public class FeuilleDeMatchAddForm extends Form {

    TextField Score = new TextField("", "Points");
    ComboBox<String> Result = new ComboBox<>();

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public FeuilleDeMatchAddForm(int id) {
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        this.setTitle("Match Sheet");
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {}
        
        Command myWorld = new Command("My World",home);
        this.getToolbar().addCommandToSideMenu(myWorld);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()==myWorld) {
                    new MyWorld().show();
                }
            }
        });
        Result.addItem("V");
        Result.addItem("D");
        this.add(Score);
        this.add(Result);
        Result.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if(Result.getSelectedItem().equals("V")){
                    ToastBar.showMessage("You picked Victory " , FontImage.MATERIAL_INFO);
                            
                }
                else{
                    ToastBar.showMessage("You picked Defeat " , FontImage.MATERIAL_INFO);
                }
            }
        }
                
                
              
        );
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        Image img = FontImage.createMaterial(FontImage.MATERIAL_ADD, s);
        Button bt = new Button("Add", img);
        this.add(bt);
        bt.addActionListener((ActionListener) (ActionEvent evt) -> {
            ToastBar.showMessage("Match Sheet added with success", FontImage.MATERIAL_INFO);
            new FeuilleDeMatchDAO().add(Score.getText(), Result.getSelectedItem(), id);
            new MatchNationalDAO().find();
        });

        Image img1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        Button bt1 = new Button("Back", img1);
        this.add(bt1);
        bt1.addActionListener((ActionListener) (ActionEvent evt) -> {
            new MatchNationalDAO().find();
        });
    }

}
