/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.FeuilleDeMatchDAO;
import DAO.MatchNationalDAO;
import com.codename1.capture.Capture;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.util.Callback;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 *
 * @author admin
 */
public class NewsfeedForm extends BaseForm {

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public NewsfeedForm(Resources res) {

        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Home");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        /**
         * ************************
         */

        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        Image camera = FontImage.createMaterial(FontImage.MATERIAL_CAMERA_ALT, s);
        getToolbar().addCommandToRightBar("", camera, (ev) -> {

            // String i = Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
            String picture = Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
            /*if (i != null) {
                try {
                    int width = Display.getInstance().getDisplayWidth();
                    Image img = Image.createImage(i);
                    Image roundMask = Image.createImage(width, img.getHeight(), 0xff000000);
                    Graphics gr = roundMask.getGraphics();
                    gr.setColor(0xffffff);
                    gr.fillArc(0, 0, width, width, 0, 360);
                    Object mask = roundMask.createMask();
                    img = img.applyMask(mask);
                    lb.setIcon(img);
                    f.revalidate();
                    
                } catch (IOException ex) {
                }
            }*/
            /**
             * ********************************
             */
            final Callback<String> resultURL = null;
            if (picture != null) {
                String filestack = "https://www.filestackapi.com/api/store/S3?key=Aj51X8uxQau85NOQU0ixAz&filename=myPicture.jpg";
                MultipartRequest request = new MultipartRequest() {
                    @Override
                    protected void readResponse(InputStream input) throws IOException {
                        JSONParser jp = new JSONParser();
                        Map<String, Object> result = jp.parseJSON(new InputStreamReader(input, "UTF-8"));
                        String url = (String) result.get("url");
                        if (url == null) {
                            System.out.println("hello");
                            resultURL.onError(null, null, 1, result.toString());
                            return;
                        }
                        resultURL.onSucess(url);
                    }
                };
                request.setUrl(filestack);
                try {
                    request.addData("fileUpload", picture, "image/jpeg");
                    request.setFilename("fileUpload", "hi.jpg");
                    NetworkManager.getInstance().addToQueue(request);
                } catch (IOException err) {
                }
            }

        });

        /**
         * *************************
         */
        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("Federer1.jpg"), spacer1, "15 Likes  ", "85 Comments", "ROGER FEDERER HOPES TO PLAY AT LEAST TWO OR THREE MORE YEARS");
        addTab(swipe, res.getImage("murray.jpg"), spacer2, "100 Likes  ", "66 Comments", "HELLO, SIR! ANDY MURRAY RECEIVES KNIGHTHOOD FROM QUEEN ELIZABETH II");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xff0000);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }

        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("National Matchs", barGroup);
        all.setUIID("SelectBar");
        all.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                 new MatchNationalDAO().findAll();
            }
        });
        
       
        RadioButton featured = RadioButton.createToggle("Match Sheet", barGroup);
        featured.setUIID("SelectBar");
        featured.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
//                new FeuilleDeMatchDAO().find();
            }
        });

        Label arrow = new Label(res.getImage("arrow.png"), "Container");

        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(4, all, featured),
                FlowLayout.encloseBottom(arrow)
        ));

        all.setSelected(true);
        arrow.setVisible(false);
        addShowListener(e -> {
            arrow.setVisible(true);
            updateArrowPosition(all, arrow);
        });
        bindButtonSelection(all, arrow);
        bindButtonSelection(featured, arrow);

        // special case for rotation
        addOrientationListener(e -> {
            updateArrowPosition(barGroup.getRadioButton(barGroup.getSelectedIndex()), arrow);
        });

        addButton(res.getImage("teenn.png"), "You always want to win. That is why you play tennis, because you love the sport and try to be the best you can at it. \nRoger Federer", false, 26, 32);
        addButton(res.getImage("tennis.jpg"), "AllWin community are pleased to offer the official Android app for the Tennis Championships. \nFollow the action LIVE during the tournament..", true, 15, 21);
        addButton(res.getImage("tenniss.jpg"), "ROGER FEDERER HOPES TO PLAY AT LEAST TWO OR THREE MORE YEARS.", false, 36, 15);
        addButton(res.getImage("novak.png"), "Djokovic has won 12 Grand Slam singles titles, the fourth most in history, and held the No. 1 spot in the ATP rankings for a total of 223 weeks.", false, 11, 9);
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }

    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
        if (img.getHeight() < size) {
            img = img.scaledHeight(size);
        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
        }
        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");

        Container page1
                = LayeredLayout.encloseIn(
                        image,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY(
                                        new SpanLabel(text, "LargeWhiteText"),
                                        FlowLayout.encloseIn(likes, comments),
                                        spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, int likeCount, int commentCount) {
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");
        Container cnt = BorderLayout.west(image);
        cnt.setLeadComponent(image);
        TextArea ta = new TextArea(title);
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(likeCount + " Likes  ", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label comments = new Label(commentCount + " Comments", "NewsBottomLine");
        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, comments)
                ));
        add(cnt);
        image.addActionListener(e -> ToastBar.showMessage(title, FontImage.MATERIAL_INFO));
    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }
}
