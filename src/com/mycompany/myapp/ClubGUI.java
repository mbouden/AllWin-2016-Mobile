/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.LienStreamingDAO;
import DAO.MatchAmateurDao;
import DAO.PariDAO;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;

/**
 *
 * @author Bilel
 */
public class ClubGUI extends Form {

    public ClubGUI() {

        Resources r = null;
        try {
            r = Resources.open("/theme.res");
        } catch (IOException ex) {
        }
        UIBuilder ui = new UIBuilder();
        
        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {}
        
        Command myWorld = new Command("My World",home);
        this.getToolbar().addCommandToSideMenu(myWorld);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand()==myWorld) {
                    new MyWorld().show();
                }
            }
        });
    }

}
