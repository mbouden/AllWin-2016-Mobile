/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Button;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import java.io.IOException;

/**
 *
 * @author admin
 */
public class SmsForm extends Form {
    
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public SmsForm() {
        
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        this.setTitle("SMS");
        TextArea Message = new TextArea("Your Message");
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        Image img = FontImage.createMaterial(FontImage.MATERIAL_SMS, s);
        Button bt = new Button("Send", img);
        add(Message);
        add(bt);
        bt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String msg=Message.getText();
                try {
                    Display.getInstance().sendSMS("+21652977996", msg);
                } catch (IOException ex) {
                }
            }
        });
        Image img1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        Button bt1 = new Button("Back", img1);
        this.add(bt1);
        bt1.addActionListener((ActionListener) (ActionEvent evt) -> {
            new MyWorld();
        });
        
        
    }
}
