/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.ContacterDAO;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;

/**
 *
 * @author admin
 */
public class ContacterAddForm extends Form {
    TextField Name = new TextField("", "Name");
    TextField Email = new TextField("", "Email");
    TextArea Message = new TextArea("Your Message");

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public ContacterAddForm() {
        setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        setTitle("Contact");
        add(Name);
        add(Email);
        add(Message);
        
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        Image img = FontImage.createMaterial(FontImage.MATERIAL_ADD, s);
        Button bt = new Button("Add", img);
        this.add(bt);
        bt.addActionListener((ActionListener) (ActionEvent evt) -> {
            ToastBar.showMessage("Message added with success", FontImage.MATERIAL_INFO);
            new ContacterDAO().add(Name.getText(), Email.getText(), Message.getText());
            new MyWorld();
        });
        Image img1 = FontImage.createMaterial(FontImage.MATERIAL_ARROW_BACK, s);
        Button bt1 = new Button("Back", img1);
        this.add(bt1);
        bt1.addActionListener((ActionListener) (ActionEvent evt) -> {
            new MyWorld();
        });
    }
    
}
