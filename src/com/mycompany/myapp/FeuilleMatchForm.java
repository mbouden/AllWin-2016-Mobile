/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.MatchNationalDAO;
import DAO.PariDAO;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;

/**
 *
 * @author admin
 */
public class FeuilleMatchForm extends Form {

    public FeuilleMatchForm() {
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        this.setTitle("Match Sheets");

        Image home = null;
        try {
            home = Image.createImage("/tennis-court.png");
        } catch (IOException ex) {
        }

        Command myWorld = new Command("My World", home);
        this.getToolbar().addCommandToSideMenu(myWorld);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == myWorld) {
                    new MyWorld().show();
                }
            }
        });
        
        Image quiz = null;
        try {
            quiz = Image.createImage("/faq.png");
        } catch (IOException ex) {
        }
        
        Command cmdSheet = new Command("Waiting Sheets", quiz);
        this.getToolbar().addCommandToSideMenu(cmdSheet);
        this.addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == cmdSheet) {
                    new MatchNationalDAO().find();
                }
            }
        });
    }

}
