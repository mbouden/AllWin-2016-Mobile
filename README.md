This project is an attempt out of Esprit Engineering School to introduce Tennis to people and enhances its Fan-base.<br> This project is dedicated to the Tunisian Federation of Tennis as well as the fans, referees, medical staff, and tennis players.
The major functionnalities in this mobile application are:<br>
    * Weekly Bets, which concerns fans only.<br>
    * Streaming live tennis matches.<br>
    * Referees are able to fill their match sheets.<br>
    * Medical staff are able to fill their reports.<br>
    * Players are able to subscribe into Clubs or Comptetions...<br>
And other functionnalities that you'd like to discover.
<br>
This mobile application was made by Dev-Squad using Codename One as cross-platform mobile application builder.<br>
Have Fun.